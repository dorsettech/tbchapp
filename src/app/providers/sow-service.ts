/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 19/06/2021 - 15:27:22
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 19/06/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AppConfig } from '../app-config';
import { map, catchError, timeout } from 'rxjs/operators';
import { HttpClient,HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { jsonpFactory } from '@angular/http/src/http_module';
import { HTTP } from '@ionic-native/http/ngx';
import { LoadingController } from "@ionic/angular";

/*
  Generated class for the SowService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable({
    providedIn: 'root',
})
export class SowService {
    cachedData: any;
    constructor(
        public http: Http,
        public appConfig: AppConfig,
        public nativeHttp: HTTP,
        public loadingCtrl: LoadingController,
        private httpClient: HttpClient
    ) {

    }
    login(params) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/sow/v1/user_login/";
        url = this.initUrl(url, params);
        return this.httpClient.get(url)

    }

    register(params) {
        console.log(params)
        var service = this;
        let url = service.appConfig.Shop_URL + "wp-json/wp/v2/register";
        url = this.initUrl(url, params);
        console.log(url)
        return this.httpClient.get(url)
        var headers = new Headers();

    }
   

    

    getVendorList() {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/sow/v1/get_vendors/";
        url = this.initUrl(url, '');
        return this.httpClient.get(url)

    }

    searchVendor(params) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/sow/v1/search_vendors/";
        url = this.initUrl(url, params);
        return this.httpClient.get(url)

    }

    getVendorDetails(vid, params) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/sow/v1/get_vendor_details/" + vid;
        url = this.initUrl(url, params);
        return this.httpClient.get(url)

    }

    initUrl(url, params) {
        if (params) {
            var keys = Object.keys(params);

            for (var i = 0; i < keys.length; i++) {
                if (i == 0) {
                    url += "?" + keys[i] + '=' + params[keys[i]];
                } else {
                    url += '&' + keys[i] + '=' + params[keys[i]];
                }
            }
        }
        return url;
    }
}
