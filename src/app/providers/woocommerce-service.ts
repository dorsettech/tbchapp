/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 18/06/2021 - 10:23:31
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 18/06/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Injectable } from "@angular/core";
import { Http, Headers, } from "@angular/http";
import { LoadingController } from "@ionic/angular";
import { AppConfig } from "../app-config";
import { map, catchError, timeout } from "rxjs/operators";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { Storage } from "@ionic/storage";
import * as CryptoJS from "crypto-js";
import { HTTP } from "@ionic-native/http/ngx";

/*
  Generated class for the WoocommerceService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable({
    providedIn: "root",
})
export class WoocommerceService {
    cachedData: any;
    public mode: any = "dark";
    public darkmode: any = "app";
    token: any;
    constructor(
        public appConfig: AppConfig,

        public nativeHttp: HTTP,
        public loadingCtrl: LoadingController,
        private httpClient: HttpClient,
        private storage:Storage
    ) {
        this.storage.get("token").then((val) => {
                    //console.log("value", val);
                    if (val) {
                        //console.log(val);
                        this.token = val;
                        console.log( this.token)
                       
                    }
                });
     }

    async presentloading() {
        var loadingModal = await this.loadingCtrl.create({
            message: 'Loading...'
        });
        loadingModal.present();
    }
    async dismissloadings() {
        var loadingModal = await this.loadingCtrl.create({
            message: 'Loading...'
        });
        loadingModal.dismiss();
    }

    dismissloading() {
        this.loadingCtrl.getTop().then((val) => {
            if (val) {
                val.dismiss();
            }
        })
    }
    getStoreInfo() {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/sow/v1/home_slider/"
        url = this.initUrl(url,'');

        return this.httpClient.get(service.initRequest(url, "get"));
    }
    getHomeSlider() {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/sow/v1/home_slider/"
        url = this.initUrl(url,'');
      
        return this.httpClient.get(service.initRequest(url, "get"));
    }
    shipingDetails(products: any){
        console.log(products)
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/get-single-product-shipping";
        url = this.initUrl(url, products);
        console.log(url)
        return this.httpClient.post(service.initRequest(url, "post"), products,)
        
    }
    shipingDetailsbyProductId(products: any){
        console.log(products)
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/get-product-shipping";
        url = this.initUrl(url, products);
        console.log(url)
        return this.httpClient.post(service.initRequest(url, "post"), products,)
        
    }
    getShippingMethod() {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/sow/v1/shipping_method/"
       
        url = this.initUrl(url,'');
        return this.httpClient.get(service.initRequest(url, "get"));

    }

    

    getProducts(products: any, searchBy: any) {
        var service = this;
        
        var url = service.appConfig.Shop_URL + (searchBy == 'product' ? "/wp-json/wc/v3/products" : "/wp-json/wp/v2/get_store_vendors_list");
        url = this.initUrl(url, products);
        return this.httpClient.get(service.initRequest(url, "get"));

    }
    shipingmethod(products: any){
        var service = this;
        var url = service.appConfig.Shop_URL + "/wp-json/wp/v2/get_per_product_shipping";
        url = this.initUrl(url, products);
        return this.httpClient.get(service.initRequest(url, "get")); 
    }
    shopList(products) {
        console.log(products)
        var service = this;
        var url = service.appConfig.Shop_URL + "/wp-json/wcfmmp/v1/store-vendors";
        url = this.initUrl(url, products);
        return this.httpClient.get(service.initRequest(url, "get"));
    }
    shopProductList(id: any) {
        console.log('id:: ', id);
         var service = this;
        var url = service.appConfig.Shop_URL + "/wp-json/wcfmmp/v1/store-vendors/" + id.vendor_id + "/products";
        url = this.initUrl(url, {});
        return this.httpClient.get(service.initRequest(url, "get"));
    }

    shopCategires(products) {
        var service = this;
        var url = service.appConfig.Shop_URL + "/wp-json/wc/v3/products/categories";
        url = this.initUrl(url, products);
        return this.httpClient.get(service.initRequest(url, "get"));
    }
    shopSearch(products) {
        var service = this;
        var url = service.appConfig.Shop_URL + "/wp-json/wp/v2/search_vendor";
        url = this.initUrl(url, products);
        return this.httpClient.get(url);
    }
    login(params, data) {
        console.log(params, "data", data)
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/user/login";
        url = this.initUrl(url, params);
        return this.httpClient.post(service.initRequest(url, "post"), data,)
    }
    register(params, data) {
        console.log(params)
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/register";
        url = this.initUrl(url, params);
        console.log(url)
        return this.httpClient.post(service.initRequest(url, "post"), data,)
        var headers = new Headers();
    }

    getListProducts(params: any) {
        var service = this;
        var url = service.appConfig.Shop_URL + "/wp-json/wp/v2/pages/category-products/";
        url = this.initUrl(url, params)
        return this.httpClient.get(service.initRequest(url, "get"));
    }

    getSingleProduct(id) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wc/v3/products/" + id;
        return this.httpClient.get(service.initRequest(url, "get"))
    }
    forgot(data,params){
        console.log(params, "data", data)
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/forgot/password";
        url = this.initUrl(url, params);
        return this.httpClient.post(service.initRequest(url, "post"), data,)
    }

    getStoreCategories(params: any) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/menu";
        url = this.initUrl(url, params)
        //console.log(url)
        return this.httpClient.get(service.initRequest(url, "get"));


    }

    createOrder(params, data) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wc-api/v3/orders";
        url = this.initUrl(url, params);

        return this.httpClient.post(service.initRequest(url, "post"), data,)

    }

    createCustomer(params, data) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wc-api/v3/customers";
        url = this.initUrl(url, params);
        console.log("url_new::", url)
        return this.httpClient.post(service.initRequest(url, "post"), data,)

    }

    getCustomerByEmail(params) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wc/v1/customers/";
        url = this.initUrl(url, params);
        return this.httpClient.get(service.initRequest(url, "get"))

    }

    getOrderList(params) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wc/v3/orders/";
        url = this.initUrl(url, params);
        return this.httpClient.get(service.initRequest(url, "get"))

    }

    getOrder(id) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wc/v3/orders/" + id;
        url = this.initUrl(url, "");
        return this.httpClient.get(service.initRequest(url, "get"))

    }

    updateOrderStatus(id, status) {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wc/v1/orders/" + id;
        url = this.initUrl(url, "");
        return this.httpClient.post(service.initRequest(url, "post"), { status: status },)

    }
    Payment(cardDetail) {
       
        console.log(cardDetail)
        var service = this;
        // let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/stripe_payment";
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/stripe_payment_test";
        console.log(url)
        // const headers = new HttpHeaders({
        //     'Content-Type': 'application/json',
        //     'Authorization': 'Bearer ' + this.token
        // });
        // const options = { headers: headers };
        url = this.initUrl(url, "");
        return this.httpClient.post(service.initRequest(url, "post"), cardDetail,)
    }

    checkToken(token){
        var service = this;
        // https://thebritishcrafthouse.co.uk/wp-json/wp/v2/token-vaildation
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/token-vaildation";
       const headers = new HttpHeaders({
           'Content-Type': 'application/json',
           'Authorization': 'Bearer ' + this.token
       });
       const options = { headers: headers };
       url = this.initUrl(url, "");
       return this.httpClient.post(service.initRequest(url, "post"), '',options)
    }


    initUrl(url, params) {
        if (params) {
            var keys = Object.keys(params);

            for (var i = 0; i < keys.length; i++) {
                if (i == 0) {
                    url += "?" + keys[i] + "=" + params[keys[i]];
                } else {
                    url += "&" + keys[i] + "=" + params[keys[i]];
                }
            }
        }
        return url;
    }

    initRequest(url: string, method) {
        if (this.isSSL(url)) {
            if (url.indexOf("?") >= 0) {
                url +=
                    "&consumer_key=" +
                    this.appConfig.Shop_ConsumerKey +
                    "&consumer_secret=" +
                    this.appConfig.Shop_ConsumerSecret;
            } else {
                url +=
                    "?consumer_key=" +
                    this.appConfig.Shop_ConsumerKey +
                    "&consumer_secret=" +
                    this.appConfig.Shop_ConsumerSecret;
            }
            return url;
        } else {
            let initParams: any = {};
            let retParams: any = {};
            initParams.url = url;
            initParams.method = method;
            initParams.data = {
                oauth_consumer_key: this.appConfig.Shop_ConsumerKey,
                oauth_nonce: this.getNonce(),
                oauth_signature_method: this.appConfig.Shop_Signature_Method,
                oauth_timestamp: this.getTimeStamp(),
            };

            retParams.oauth_consumer_key = initParams.data.oauth_consumer_key;
            retParams.oauth_nonce = initParams.data.oauth_nonce;
            retParams.oauth_signature_method = initParams.data.oauth_signature_method;
            retParams.oauth_timestamp = initParams.data.oauth_timestamp;
            retParams.oauth_signature = this.authorize(initParams);
            if (url.indexOf("?") >= 0) {
                url += "&";
            } else {
                url += "?";
            }
            return (url +=
                "oauth_consumer_key=" +
                initParams.data.oauth_consumer_key +
                "&oauth_nonce=" +
                initParams.data.oauth_nonce +
                "&oauth_signature_method=" +
                initParams.data.oauth_signature_method +
                "&oauth_timestamp=" +
                initParams.data.oauth_timestamp +
                "&oauth_signature=" +
                encodeURIComponent(this.authorize(initParams)));
        }
    }

    isSSL(str) {
        var tarea = str;
        var tarea_regex = /^(https)/;
        if (tarea_regex.test(String(tarea).toLowerCase()) == true) {
            return true;
        }
        return false;
    }

    getNonce() {
        var word_characters =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var result = "";

        for (var i = 0; i < this.appConfig.Shop_Nonce_Length; i++) {
            result +=
                word_characters[
                parseInt(String(Math.random() * word_characters.length), 10)
                ];
        }

        return result;
    }

    getTimeStamp() {
        return parseInt(String(new Date().getTime() / 1000), 10);
    }

    randomJsonpName() {
        var str;
        str = new Date().getTime() + Math.round(Math.random() * 1000000);
        return str;
    }

    authorize(request) {
        if (!request.data) {
            request.data = {};
        }
        let oauth_signature = this.getSignature(request);
        return oauth_signature;
    }

    hash(base_string, key) {
        switch (this.appConfig.Shop_Signature_Method) {
            case "HMAC-SHA1":
                return CryptoJS.HmacSHA1(base_string, key).toString(
                    CryptoJS.enc.Base64
                );
            case "HMAC-SHA256":
                return CryptoJS.HmacSHA256(base_string, key).toString(
                    CryptoJS.enc.Base64
                );
        }
    }

    getSignature(request) {
        return this.hash(
            this.getBaseString(request),
            this.percentEncode(this.appConfig.Shop_ConsumerSecret) + "&"
        );
    }

    getBaseString(request) {
        return (
            request.method.toUpperCase() +
            "&" +
            this.percentEncode(this.getBaseUrl(request.url)) +
            "&" +
            this.percentEncode(this.getParameterString(request))
        );
    }

    getParameterString(request) {
        var base_string_data = this.sortObject(
            this.percentEncodeData(
                this.mergeObject(request.data, this.deParamUrl(request.url))
            )
        );
        var data_str = "";

        for (var key in base_string_data) {
            var value = base_string_data[key];
            if (value && Array.isArray(value)) {
                value.sort();
                var valString = "";
                value.forEach(
                    function (item, i) {
                        valString += key + "=" + item;
                        if (i < value.length) {
                            valString += "&";
                        }
                    }.bind(this)
                );
                data_str += valString;
            } else {
                data_str += key + "=" + value + "&";
            }
        }

        data_str = data_str.substr(0, data_str.length - 1);

        return data_str;
    }

    mergeObject(obj1, obj2) {
        var merged_obj = obj1;
        for (var key in obj2) {
            merged_obj[key] = obj2[key];
        }
        return merged_obj;
    }

    deParam(param) {
        var arr = param.split("&");
        var data = {};

        for (var i = 0; i < arr.length; i++) {
            var item = arr[i].split("=");
            data[item[0]] = decodeURIComponent(item[1]);
        }
        return data;
    }

    deParamUrl(url) {
        var tmp = url.split("?");

        if (tmp.length === 1) return {};

        return this.deParam(tmp[1]);
    }

    percentEncodeData(data) {
        var result = {};

        for (var key in data) {
            var value = data[key];
            if (value && Array.isArray(value)) {
                var newValue = [];
                value.forEach(
                    function (val) {
                        newValue.push(this.percentEncode(val));
                    }.bind(this)
                );
                value = newValue;
            } else {
                value = this.percentEncode(value);
            }
            result[this.percentEncode(key)] = value;
        }

        return result;
    }

    getBaseUrl(url) {
        return url.split("?")[0];
    }

    percentEncode(str) {
        return encodeURIComponent(str)
            .replace(/\!/g, "%21")
            .replace(/\*/g, "%2A")
            .replace(/\'/g, "%27")
            .replace(/\(/g, "%28")
            .replace(/\)/g, "%29");
    }

    sortObject(data) {
        var keys = Object.keys(data);
        var result = {};

        keys.sort();

        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            result[key] = data[key];
        }

        return result;
    }


    getMainPageProducts() {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/get/here_today_gone_tom_using_slider"
        url = this.initUrl(url, {})
        return this.httpClient.get(service.initRequest(url, "get"));
    }

    getGiftProducts() {
        var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/pages/home-page"
        url = this.initUrl(url, {})
        return this.httpClient

            .post(
                service.initRequest(url, "post"),
                { status: status },

            )

    }
    getHomeProducts(params) {
        console.log(params)
        var service = this;
        var url = service.appConfig.Shop_URL + "/wp-json/wp/v2/pages/" + params;
        url = this.initUrl(url, params);
        console.log(url)
        return this.httpClient.post(
            service.initRequest(url, "post"),
            { status: status },

        )
    }

    getCatProducts(params){
         console.log(params)
       var service = this;
        let url = service.appConfig.Shop_URL + "/wp-json/wp/v2/pages/"+ params;
        url = this.initUrl(url, {})
        return this.httpClient.get(service.initRequest(url, "get"));
        
    }

    get(url){
      var service = this;
       let url2 = service.appConfig.Shop_URL + url;
       url2 = this.initUrl(url2, {})
       return this.httpClient.get(service.initRequest(url2, "get"));
       
   }

    postQuery(queryData) {
        console.log("queryCheck ::", queryData)
        var service = this;
        var url = service.appConfig.Shop_URL + "/wp-json/wp/v2/post/contact";
        url = this.initUrl(url, queryData);
        console.log("url ::", url)
        return this.httpClient.post(service.initRequest(url, "post"),
            { status: status })
    }

}

export class CartProduct {
    public selectedShipping: any;
    public total:any;
    public product_id: number;
    public name: string;
    public price: any;
    public quantity: number;
    public variation_id: number;
    public variations: any;
    public variation_name: string;
    public thumb: string;
    public vendor_id: string;
    public vendor_name: string;
    public shippingData:any;
    public addons_data:any;
    public addons_data_select:any;
    public addons_data_checkbox:any;
    
    constructor() {
        this.name = "";
        this.price = 0;
        this.product_id = 0;
        this.quantity = 0;
        this.variation_id = 0;
        this.variation_name = "";
        this.thumb = "";
        this.vendor_id = "";
        this.vendor_name = "";
        this.selectedShipping = "";
        this.addons_data="";
        this.addons_data_select="";
        this.addons_data_checkbox="";
    }
}

export class WishlistProduct {
    public product_id: number;
    public price: number;
    public product_image: string;
}

export class Cart {
    public CartProductArray: Array<CartProduct>;
    public total: number;
    public totalShip: number;
}




