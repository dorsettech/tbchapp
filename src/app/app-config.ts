/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 31/05/2021 - 12:02:33
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 31/05/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
/*
  Generated class for the ShopConstant provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
export class AppConfig {
  Shop_Signature_Method = 'HMAC-SHA1';//no need change
  Shop_Nonce_Length = 32;//no need change
  Shop_Parameter_Seperator = ', ';//no need change
  Shop_Language='en';//If change this,create a new file named your language(eg.chinese:zh.json) in assets/i18n then copy all from en.json and translate it. 

  Shop_Name = "The British Craft House";//no need set,will get from your website
  Shop_Version = "1.0";
 
  // Shop_URL="https://bchaaron.honesttech.co.uk";
  Shop_URL = "https://thebritishcrafthouse.co.uk";//set your website url,eg:"http://www.yousite.com"
  //test_URL ="https://dev.thebritishcrafthouse.co.uk/";
  test_URL = "https://bchaaron.honesttech.co.uk";
  Shop_ConsumerKey = "ck_1c92b6c760b9f27697c03d2e9c0219be981f7bbd";//woocommerce rest api ConsumerKey
  Shop_ConsumerSecret = "cs_0cd584bfa6f2f8b4ce31881aa41f0a256dea4b81";//woocommerce rest api ConsumerSecret
  
  Shop_Currency = "";//no need setting,will get from your Website
  Shop_Currency_format = "";//no need setting,will get from your Website

  //your shipping method,you need set these method with our plugin
  Shop_Shipping = [];

  App_Secret = "f5e379e9020a2a493e4bd23ec3c770a0fd1303b235a7d7723059d24e929a0c06";//install our plugin then Generate Secret key in basic setting 
  Show_Home_Slide = true;//whether show home slide

  //Paypal setting
  //PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
  //if you want to test payment,set Environments to PayPalEnvironmentSandbox,otherwise set to PayPalEnvironmentProduction
  Paypal_Environments = "";
  PayPal_EnvironmentProduction = "";
  PayPal_EnvironmentSandbox = "";

  //Stripe payment setting
//   Enable_Stripe = false;//Enable or disable stripe
//   Stripe_Live_Secret_Key = "sk_live_YqeEbhkB0OrmlkA7MxK4aXHH00yDdVG477";//Live mode Secret Key
//   Stripe_Live_Publishable_Key = "pk_live_Bj4ZFQJoi3Z3M4eYh5zq6K8000SAuK0tYb";//Live mode Publishable Key
  // Enable_Stripe = false;//Enable or disable 
 
  Stripe_Live_Secret_Key = "sk_live_YqeEbhkB0OrmlkA7MxK4aXHH00yDdVG477";//Live mode Secret Key
  Stripe_Live_Publishable_Key = "pk_live_Bj4ZFQJoi3Z3M4eYh5zq6K8000SAuK0tYb";//Live mode Publishable Key

  Enable_Stripe_Test_Mode = false;//Enable or disable test mode
/////////////////////////////////
  Stripe_Test_Secret_Key = "sk_test_4A5Us3O1KqJaYBrC0J69pDbn00G4Iq3MKJ";//Test mode Secret Key
  Stripe_Test_Publishable_Key = "pk_test_FvWubPv9FF6hXHFuTvDVgd2Y00s6f1ONkq";//Test mode Publishable Key
 
///////////////////////
  //Onesignal setting
  //Please check our online document set these
  Onesignal_Enable = false;//enable/disable Onesignal
  OneSignal_AppID = "87516daa-6394-4a13-a74f-d925a1618c25";
  GCM_SenderID = "AIzaSyCiK6UZ9cjxF7GDfzHdYgRjcn8-fnktUlI";

  //enable login with password,need to add codes to woocommerce,please check readme file
  Eanble_Login_With_Password = true;

  //Contact page info
  Service_In_Weekdays = "Monday-Friday (9am - 4pm)";
  Service_Weekend = "Saturday-Sunday (12pm - 4pm)";
  Service_Tel = "081123456";

  //Faq page info
  //Title:question Title
  //content:the answser
  Question_Array = [
    {
      'Title': 'Dicit debitis at sed?',
      Content: `Movet apeirian verterem eu quo, vix elit voluptatum te, has ea solum viris audiam. Mel ex suas fugit altera. Amet
          soluta quo id, hinc adhuc alterum nam ad. Qui in natum laudem fabulas.` },
    {
      'Title': 'Dicit debitis at sed?',
      Content: `Movet apeirian verterem eu quo, vix elit voluptatum te, has ea solum viris audiam. Mel ex suas fugit altera. Amet
          soluta quo id, hinc adhuc alterum nam ad. Qui in natum laudem fabulas.` },
    {
      'Title': 'Dicit debitis at sed?',
      Content: `Movet apeirian verterem eu quo, vix elit voluptatum te, has ea solum viris audiam. Mel ex suas fugit altera. Amet
          soluta quo id, hinc adhuc alterum nam ad. Qui in natum laudem fabulas.` },
  ];

  //About page info
  Introduction = `Movet apeirian verterem eu quo, vix elit voluptatum te, has ea solum viris audiam. Mel ex suas fugit altera. Amet soluta
    quo id, hinc adhuc alterum nam ad. Qui in natum laudem fabulas.`;
  Address1 = "123 N Harbor Dr. Redondo Beach, CA";
  Address2 = "United States 123456";
  CopyRight = "@SowWcVendorStore";

  //logo in app not app icon
  Logo_Image = "assets/img/logo.png";//copy your own image to assets/img/yourlogo.png and set logo.png to yourlogo.png. 

  constructor() {
  }



}
