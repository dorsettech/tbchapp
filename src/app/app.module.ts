import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { WoocommerceService } from './providers/woocommerce-service';
import { CountryService } from './providers/country-service';
import { TbarService } from './providers/tbar-service';
import { UserService } from './providers/user-service';
import { SowService } from './providers/sow-service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AppConfig } from './app-config';
import { HttpModule } from '@angular/http';
import { LoginPageModule } from './pages/login/login.module';
import { AboutPageModule } from './pages/about/about.module';
import { FaqPageModule } from './pages/faq/faq.module';
import { ContactPageModule } from './pages/contact/contact.module';
import { RegisterPageModule } from './pages/register/register.module';
import { ForgetPageModule } from './pages/forget/forget.module';
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { ThanksPageModule } from './pages/thanks/thanks.module';

import { SlugUrlPageModule } from './pages/slug-url/slug-url.module';
import { CountryPageModule } from './pages/country/country.module';
import { StripePayPageModule } from './pages/stripe-pay/stripe-pay.module';
import { HTTP } from '@ionic-native/http/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
//import { LazyLoadImageModule } from 'ng-lazyload-image';
import { AttributePage } from './pages/attribute/attribute';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { ThemeDetection, ThemeDetectionResponse } from '@ionic-native/theme-detection/ngx';
import { Network } from '@ionic-native/network/ngx';

@NgModule({
    declarations: [AppComponent, AttributePage],
    entryComponents: [AttributePage],
    imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
        HttpClientModule, HttpModule, LoginPageModule, RegisterPageModule, AboutPageModule, FaqPageModule,
        ThanksPageModule,
       // LazyLoadImageModule,
        CountryPageModule,
        StripePayPageModule,
        ForgetPageModule,
        ContactPageModule,
        IonicStorageModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })],
    providers: [
        Camera,
        StatusBar,
        ThemeDetection,
        SplashScreen, Keyboard,
        WoocommerceService, AppConfig, CountryService, TbarService, UserService, SowService, InAppBrowser, HTTP, OneSignal, PayPal,Network,
        SocialSharing,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, "/assets/i18n/", ".json");
}

