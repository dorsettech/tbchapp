import { Component, OnInit } from '@angular/core';
import {
    ModalController,
    MenuController
} from "@ionic/angular";
@Component({
    selector: 'app-learn-more',
    templateUrl: './learn-more.page.html',
    styleUrls: ['./learn-more.page.scss'],
})
export class LearnMorePage implements OnInit {

    constructor(public modalCtrl: ModalController, public menuCtrl: MenuController,) { }

    ngOnInit() {
    }
    close() {
        this.modalCtrl.dismiss();
    }
    openSidemenu() {
        this.menuCtrl.open();
    }
}
