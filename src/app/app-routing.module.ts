import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [

    { path: '', loadChildren: './pages/main/main.module#MainPageModule' },
    { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
    { path: 'details/:id', loadChildren: './pages/product-details/product-details.module#ProductDetailsPageModule' },
    { path: 'checkout', loadChildren: './pages/checkout/checkout.module#CheckoutPageModule' },
    { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
    { path: 'address', loadChildren: './pages/address/address.module#AddressPageModule' },
    { path: 'contact', loadChildren: './pages/contact/contact.module#ContactPageModule' },
    { path: 'faq', loadChildren: './pages/faq/faq.module#FaqPageModule' },
    { path: 'listings/', loadChildren: './pages/listings/listings.module#ListingsPageModule' },
    { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
    { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
    { path: 'search', loadChildren: './pages/search/search.module#SearchPageModule' },
    { path: 'stripe', loadChildren: './pages/stripe-pay/stripe-pay.module#StripePayPageModule' },
    { path: 'thanks', loadChildren: './pages/thanks/thanks.module#ThanksPageModule' },
    { path: 'orderlist', loadChildren: './pages/order-list/order-list.module#OrderListPageModule' },
    { path: 'orderdetails/:id', loadChildren: './pages/order-details/order-details.module#OrderDetailsPageModule' },
    { path: 'cart', loadChildren: './pages/cart/cart.module#CartPageModule' },
    { path: 'main', loadChildren: './pages/main/main.module#MainPageModule' },
    { path: 'main-inner', loadChildren: './main-inner/main-inner.module#MainInnerPageModule' },
    { path: 'main-product/:slug', loadChildren: './pages/main-product/main-product.module#MainProductPageModule' },
    { path: 'learn-more', loadChildren: './learn-more/learn-more.module#LearnMorePageModule' },
    { path: 'forget', loadChildren: './pages/forget/forget.module#ForgetPageModule' },
    { path: 'shop-products', loadChildren: './pages/shop-products/shop-products.module#ShopProductsPageModule' },
    { path: 'wishlist', loadChildren: './pages/wishlist/wishlist.module#WishlistPageModule' },
  

 




    // { path: 'category/:id', loadChildren: './pages/recipe-detail/recipe-detail.module#RecipeDetailPageModule' },
    // { path: 'recipe/:id', loadChildren: './pages/recipe-list/recipe-list.module#RecipeListPageModule' },
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
