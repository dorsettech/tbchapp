/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 25/06/2021 - 09:51:47
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 25/06/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component } from "@angular/core";

import {
    Platform,
    ModalController,
    NavController,
    ToastController,
    AlertController,
} from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { TranslateService } from "@ngx-translate/core";
import { AppConfig } from "./app-config";
import { ContactPage } from "./pages/contact/contact";
import { AboutPage } from "./pages/about/about";
import { FaqPage } from "./pages/faq/faq";
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Router, NavigationExtras } from "@angular/router";
import { Storage } from "@ionic/storage";
import { UserService } from "./providers/user-service";
import { WoocommerceService } from "./providers/woocommerce-service";
import { SowService } from "./providers/sow-service";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { MenuController } from "@ionic/angular";
import { Events } from "../app/providers/events";
import { data } from "jquery";
import { Network } from '@ionic-native/network/ngx';

import {
    ThemeDetection,
    ThemeDetectionResponse,
} from "@ionic-native/theme-detection/ngx";
@Component({
    selector: "app-root",
    templateUrl: "app.component.html",
})
export class AppComponent {
    isLogOut: boolean = false;
    isSigin: boolean = true;
    temp_token: Promise<any>;
    constructor(
        private platform: Platform,

        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        translate: TranslateService,
        public appConfig: AppConfig,
        public modalCtrl: ModalController,
        public router: Router,
        public storage: Storage,
        public userService: UserService,
        public wooService: WoocommerceService,
        private oneSignal: OneSignal,
        private themeDetection: ThemeDetection,
        private menuCtrl: MenuController,
        public sowService: SowService,
        public events: Events,
        public navCtrl: NavController,
        public toastController: ToastController,
        public alertCtrl: AlertController,
        private iab: InAppBrowser,
        private network: Network
    ) {
       
        this.initializeApp();
        translate.setDefaultLang(this.appConfig.Shop_Language);
        translate.use(this.appConfig.Shop_Language);
        this.events.subscribe("logout", (data) => {
            console.log("event");
            setTimeout(() => {
                this.UserDetail();
            }, 200);
        });
    }



    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.UserDetail();
            this.themeDetct();
            
           // check network status
        
           if(this.userService.isAuthenticated != true){
               this.router.navigate(["login"]);
           }
           
             this.wooService.getShippingMethod().subscribe((data: any) => {
                console.log("data:: ", data);

                if (data) {
                    //console.log("getShippingMethod data:: ", data);

                    this.appConfig.Shop_Shipping = data;
                }
            });
            console.log(
                "this.wooService.getShippingMethod():: ",
                this.wooService.getShippingMethod()
            );
            if (this.appConfig.Onesignal_Enable == true) {
                this.oneSignal.startInit(
                    this.appConfig.OneSignal_AppID,
                    this.appConfig.GCM_SenderID
                );
                this.oneSignal.handleNotificationReceived().subscribe(() => {
                    // do something when notification is received
                });
                this.oneSignal.handleNotificationOpened().subscribe(() => {
                    // do something when a notification is opened
                });
                this.oneSignal.endInit();
            }

            this.wooService.getStoreInfo().subscribe((storeInfo: any) => {
                console.log("StoreInfo", storeInfo);
             
            });

            this.storage.get("oddwolves-user-info").then((data) => {
                if (data) {
                    var userInfo = data;
                    this.userService.id = userInfo.id;
                    this.userService.email = userInfo.email;
                    this.userService.first_name = userInfo.first_name;
                    this.userService.last_name = userInfo.last_name;
                    this.userService.name = userInfo.first_name + userInfo.last_name;
                    this.userService.username = userInfo.username;
                    this.userService.isAuthenticated = true;
                }
            });
            this.platform.resume.subscribe(async () => {
                this.themeDetct();
            });
        });
    }
    themeDetct() {
        var that = this;
        this.themeDetection
            .isAvailable()
            .then((res: ThemeDetectionResponse) => {
                if (res.value) {
                    this.wooService.darkmode = "default";
                    this.themeDetection
                        .isDarkModeEnabled()
                        .then((res: ThemeDetectionResponse) => {
                            console.log("isDarkModeEnabled", res);
                            if (res.value) {
                                that.wooService.mode = "dark";
                                localStorage.setItem("darkMode", "true");
                                document.body.classList.add("dark");
                            } else {
                                that.wooService.mode = "light";
                                localStorage.setItem("darkMode", "false");
                                document.body.classList.remove("dark");
                            }
                        })
                        .catch((error: any) => console.error(error));
                } else {
                    this.wooService.darkmode = "app";
                }
            })
            .catch((error: any) => console.error(error));
    }
    modalContact() {
        this.menuCtrl.close();
        this.router.navigate(["contact"]);
    }
   
    modalFaq() {
        this.menuCtrl.close();
        this.router.navigate(["faq"]);
    }
    signin_con() {
        this.menuCtrl.close();
        let NavigationExtras: NavigationExtras = {
            queryParams: {
                value: "login",
            },
        };

        // };this.router.navigate(['login'],NavigationExtras)
        this.navCtrl.navigateForward("login", NavigationExtras);
        // this.router.navigate(['login']);
    }


    async logout() {
        localStorage.removeItem('oddwolves-cart');
        let alert = await this.alertCtrl.create({
            header: "Are you sure",
            mode:'ios',
            message: "You want to remove this product from cart?",
            buttons: [
                {
                    text: "No",
                    handler: async () => {
                        console.log("Cancel clicked");

                        this.storage.remove("oddwolves-user-info");
                        this.menuCtrl.close();
                        this.userService.id = "";
                        this.userService.email = "";
                        this.userService.first_name = "";
                        this.userService.last_name = "";
                        this.userService.name = "";
                        this.userService.username = "";
                        this.userService.isAuthenticated = false;
                        const toast = await this.toastController.create({
                            message: "Successfully Logged Out",
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                        this.events.publish("logout", {});
                        this.navCtrl.navigateRoot(["/main"]);
                    },
                },
                {
                    text: "Yes",
                    handler: async () => {
                        this.storage.clear();
                        this.menuCtrl.close();
                        this.userService.id = "";
                        this.userService.email = "";
                        this.userService.first_name = "";
                        this.userService.last_name = "";
                        this.userService.name = "";
                        this.userService.username = "";
                        this.userService.isAuthenticated = false;
                        const toast = await this.toastController.create({
                            message: "Successfully Logged Out",
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                        this.events.publish("logout", {});
                        this.navCtrl.navigateRoot(["/main"]);
                    },
                },
            ],
        });

        await alert.present();
    }

    goHome() {
        this.menuCtrl.close();
        this.router.navigate(["/tabs/category"]);
    }
    goMain() {
        this.menuCtrl.close();
        this.router.navigate(["/main"]);
    }
    cart() {
        this.menuCtrl.close();
        this.router.navigate(["/cart"]);
    }
    UserDetail() {
        this.storage.get("oddwolves-user-info").then((val) => {
            if (val) {
                this.isLogOut = true;
                this.isSigin = false;
            } else {
                this.isLogOut = false;
                this.isSigin = true;
            }
        });
    }

    openWebpage() {
        const options: InAppBrowserOptions = {
            zoom: 'no'
        }
      const browser = this.iab.create('https://thebritishcrafthouse.co.uk/sell-with-us/', '_system', options);

    }
}
