import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CountryPage } from './country';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CountryPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: CountryPage
      }
    ])
  ],
})
export class CountryPageModule { }
