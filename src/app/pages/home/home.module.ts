import { NgModule } from '@angular/core';
import { IonicModule  } from '@ionic/angular';
import { HomePage } from './home';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';  

@NgModule({
  declarations: [
    HomePage
  ],
  imports: [
    IonicModule,
    CommonModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
})
export class HomePageModule { }
