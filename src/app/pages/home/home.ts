/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 25/06/2021 - 11:32:20
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 25/06/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation, ViewChild } from "@angular/core";
import {
    NavController,
    LoadingController,
    AlertController,
    ToastController,
} from "@ionic/angular";
import { WoocommerceService } from "../../providers/woocommerce-service";
import {
    IonSlides,
    IonSlide,
    IonContent,
    MenuController,
} from "@ionic/angular";
import { TbarService } from "../../providers/tbar-service";
import { SowService } from "../../providers/sow-service";
import { AppConfig } from "../../app-config";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
// import * as $ from "jquery";

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
declare var window: any;
declare var $: any;
declare var select2: any;
@Component({
    selector: "app-page-home",
    templateUrl: "home.html",
    styleUrls: ["home.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class HomePage implements OnInit {
    counties: any;
    page: number;
    per_page: number;
    has_more: boolean;
    loadingModal: any;
    errorModal: any;
    slides: Array<any>;
    load_slide_end: boolean;
    load_products_end: boolean;

    showSlide = false;
    categories: any = [];
    products: any = [];
    searchProducts: Array<any> = new Array<any>();
    searchInput: any = "";
    noSearchResult: boolean = false;
    loading = false;
    searchRequest: any;
    temp_token: any;
    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public wooService: WoocommerceService,
        private router: Router,
        public tb: TbarService,
        public alertCtrl: AlertController,
        public sowService: SowService,
        public appConfig: AppConfig,
        public translateService: TranslateService,
        public menuCtrl: MenuController,
        public storage: Storage,
        public toastController: ToastController
    ) {
        let that = this;
        window.addEventListener("keyboardWillHide", () => {
            that.searchProducts = [];
        });
    }
    @ViewChild("itemSlide") categorySlide: IonSlides;
    slideOpts = {
        slidesPerView: 3.2,
        autoplay: false,
    };

    openSidemenu() {
        this.menuCtrl.open();
    }

    ionViewWillEnter() {
        this.page = 1;
        this.per_page = 10;
        this.has_more = true;

        this.load_products_end = false;
        this.load_slide_end = false;
        // this.getProductsAndSlider();
        this.getCategories();
        this.getShopList();
        // this.storage.get("temp_token").then((val) => {
        //     //console.log("value", val);
        //     if (val) {
        //         //console.log(val);
        //         this.temp_token = val;
        //         console.log( this.temp_token)
               
        //     }
        // });
       
       
    }

    doRefresh(refresher) {
        this.page = 1;
        this.translateService
            .get(["Notice", "NetWork_Error", "OK"])
            .subscribe((value) => {
                this.wooService.shopList({ page: this.page}).subscribe(
                        (products: Array<any>) => {
                            console.log("Products: ", JSON.stringify(products));
                            this.products = products;
                            if (products.length < this.per_page) {
                                this.has_more = false;
                            } else {
                                this.page++;
                            }
                            refresher.target.complete();
                        },
                        (reson) => {
                            refresher.target.complete();
                            this.toastController.create({
                                // header: value["Notice"],
                                message: value["Network_Error"],
                                duration: 2000,
                                color: "dark",
                            });
                        }
                    );
            });
    }

    getShopList() {
        this.translateService
            .get(["Notice", "Loading", "NetWork_Error", "OK"])
            .subscribe(async (value) => {
                var loadingModal = await this.loadingCtrl.create({
                    message: value["Loading"],
                    keyboardClose: true,
                });
              
                this.wooService.shopList({ page: this.page}).subscribe(
                    (products: Array<any>) => {
                        console.log("Shops: ", products);
                        this.products = products;
                        if (products.length < this.per_page) {
                            this.has_more = false;
                        } else {
                            this.page++;
                        }
                        this.load_products_end = true;
                        loadingModal.dismiss();
                    },
                    async (reason) => {
                        loadingModal.dismiss();
                        const toast = await this.toastController.create({
                            message: value["Network_Error"],
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                    }
                );
            });
    }

    getCategories() {
        this.translateService
            .get(["Notice", "Loading", "NetWork_Error", "OK"])
            .subscribe(async (value) => {
                var loadingModal = await this.loadingCtrl.create({
                    message: value["Loading"],
                    keyboardClose: true,
                });
                this.wooService.shopCategires({ page: 1, per_page: 100 }).subscribe(
                    (categories: Array<any>) => {
                        console.log("Shops categories: ", categories);
                        this.categories = categories;
                        setTimeout(() => {
                            $("#shop-category-select").select2();
                        }, 100);
                        loadingModal.dismiss();
                    },
                    async (reason) => {
                        loadingModal.dismiss();
                        const toast = await this.toastController.create({
                            message: value["Network_Error"],
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                    }
                );
            });
    }

    getProductsAndSlider() {
        this.translateService
            .get(["Notice", "Loading", "NetWork_Error", "OK"])
            .subscribe(async (value) => {
                var loadingModal = await this.loadingCtrl.create({
                    message: value["Loading"],
                    keyboardClose: true,
                });
                this.wooService
                    .getProducts({ page: this.page, per_page: this.per_page },'product')
                    .subscribe(
                        (products: Array<any>) => {
                            console.log("Products: ", JSON.stringify(products));
                            this.products = products;
                            if (products.length < this.per_page) {
                                this.has_more = false;
                            } else {
                                this.page++;
                            }
                            this.load_products_end = true;
                            if (this.appConfig.Show_Home_Slide) {
                                if (this.load_products_end && this.load_products_end) {
                                    loadingModal.dismiss();
                                }
                            } else {
                                loadingModal.dismiss();
                            }
                        },
                        async (reason) => {
                            loadingModal.dismiss();
                            const toast = await this.toastController.create({
                                message: value["Network_Error"],
                                duration: 2000,
                                color: "dark",
                            });
                            toast.present();
                        }
                    );
            });

        if (this.appConfig.Show_Home_Slide) {
            this.wooService.getHomeSlider().subscribe((data: Array<any>) => {
                this.slides = data;
                if (this.slides.length > 0) {
                    this.showSlide = true;
                }
                this.load_slide_end = true;
                if (this.load_products_end && this.load_products_end) {
                }
            });
        }
    }

    doInfinite(infiniteScroll) {
        this.translateService
            .get(["Notice", "NetWork_Error", "OK"])
            .subscribe((value) => {
                if (this.has_more) {
                    //   this.wooService
                    //     .getProducts({ page: this.page, per_page: this.per_page })
                    this.wooService.shopList({ page: this.page }).subscribe(
                        (products: any) => {
                            products.forEach((p) => {
                                this.products.push(p);
                            });

                            if (products.length < this.per_page) {
                                this.has_more = false;
                            } else {
                                this.page++;
                            }
                            infiniteScroll.target.complete();
                        },
                        async (reason) => {
                            infiniteScroll.target.complete();
                            const toast = await this.toastController.create({
                                message: value["Network_Error"],
                                duration: 2000,
                                color: "dark",
                            });
                            toast.present();
                        }
                    );
                } else {
                    infiniteScroll.target.complete();
                    infiniteScroll.target.disabled = true;
                }
            });
    }

    viewProduct(id) {
        this.clearSearch();
        this.navCtrl.navigateForward(["shop-products"], { queryParams: { id } });
        console.log( this.navCtrl.navigateForward(["shop-products"], { queryParams: { id } }))
    }
    
    // viewProduct(id) {
    //     this.clearSearch();
    //     if (this.searchBy == 'product') {

    //         this.navCtrl.navigateForward("details/" + id);
    //     } else {
    //         this.navCtrl.navigateForward(["shop-products"], { queryParams: { id } });
    //     }
    // }

    goToSearch() {
        this.navCtrl.navigateForward("search");
    }

    OnInit() {
        this.tb.hideBar = false;
    }

    ngOnInit() { }

    onSearch(value) {
        this.searchInput = value;
        if (this.searchInput.length) {
            this.loading = true;
            this.translateService
                .get(["Notice", "Loading", "NetWork_Error", "OK"])
                .subscribe(async (value) => {
                    var loadingModal = await this.loadingCtrl.create({
                        message: value["Loading"],
                    });
                    // loadingModal.present();
                    if (this.searchRequest) {
                        this.searchRequest.unsubscribe();
                    }

                    this.searchRequest = this.wooService.shopSearch({ search_string: this.searchInput }).subscribe(
                        (searchedshops: Array<any>) => {
                            console.log("products:: ", searchedshops);
                            this.loading = false;
                            if (!searchedshops.length) {
                                this.searchProducts = [];
                                this.noSearchResult = true;
                            } else {
                                this.searchProducts = searchedshops;

                            }
                            // loadingModal.dismiss();
                        },
                        async (reason) => {
                            // loadingModal.dismiss();

                            const toast = await this.toastController.create({
                                message: value["Network_Error"],
                                duration: 2000,
                                color: "dark",
                            });
                            toast.present();
                        }
                    );
                });
        } else {
            this.searchProducts = [];
            this.loading = false;
        }
    }

    clearSearch() {
        this.searchInput = "";
        this.searchProducts = [];
    }
}
