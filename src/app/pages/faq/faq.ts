import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, MenuController } from '@ionic/angular';
import { AppConfig } from '../../app-config';
/*
  Generated class for the Faq page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-faq',
    templateUrl: 'faq.html',
    styleUrls: ['faq.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FaqPage {
    showGroup;
    isActive: any;
    type: string;
    isShow: any;
    constructor(public navCtrl: NavController, public modalView: ModalController, public menuCtrl: MenuController, public appConfig: AppConfig) {
        this.type = "buyer";
    }


    toggleGroup(num) {
        if (this.isGroupShown(num)) {
            this.showGroup = null;
        }
        else {
            this.showGroup = num;
        }
    }

    isGroupShown(num) {
        return this.showGroup == num;
    }

    close() {
        this.modalView.dismiss();
    }
    openSidemenu() {
        this.menuCtrl.open();
    }
    segmentChanged(ev: any) {
        console.log("Segment changed", ev);
    }
    buyer(val) {
        console.log(this.isActive, "++++++", val)

        if (this.isActive == val) {
            this.isActive = '';
        } else {
            this.isActive = val;
        }


    }
    seller(val) {
        this.isShow = val;
    }
}
