import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { FaqPage } from './faq';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

@NgModule({
  declarations: [
    FaqPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,ReactiveFormsModule ,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: FaqPage
      }
    ])
  ],
})
export class FaqPageModule { }
