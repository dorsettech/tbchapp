import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ListingsPage } from './listings';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule ,} from '@angular/common'; 
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    ListingsPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: ListingsPage
      }
    ])
  ],
})
export class ListingsPageModule { }
