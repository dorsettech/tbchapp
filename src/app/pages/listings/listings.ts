/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 03/06/2021 - 13:22:47
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 03/06/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation, ɵArgumentType } from '@angular/core';
import { NavController, ActionSheetController, ModalController, LoadingController, AlertController, MenuController, ToastController } from '@ionic/angular';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { TbarService } from '../../providers/tbar-service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
/*
  Generated class for the Listings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'app-page-listings',
    templateUrl: 'listings.html',
    styleUrls: ['listings.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ListingsPage {
    page: number;
    per_page: number;
    has_more: boolean;

    products: Array<any> = new Array<any>();
    categorySlug: string;
    editForm: any;
    sorting: any;
    selected:any ;
    constructor(public navCtrl: NavController, public actionSheetCtrl: ActionSheetController, public modalCtrl: ModalController, private router: Router,
        public loadingCtrl: LoadingController, public wooService: WoocommerceService, public tbarService: TbarService, public alertCtrl: AlertController
        , public translateService: TranslateService, public menuCtrl: MenuController, private activatedRoute: ActivatedRoute,
        public storage:Storage, public toastController: ToastController) {
        this.page = 1;
        this.per_page = 10;
        this.has_more = true;
        this.categorySlug = localStorage.getItem("Slug")
         //this.categorySlug = this.activatedRoute.snapshot.paramMap.get('catID');
        // this.storage.set('categorySlug', this.activatedRoute.snapshot.paramMap.get('catID'));
        this.selected =  'defult'
        console.log(this.categorySlug)
        this.editForm = new FormGroup({
            sorting: new FormControl("", Validators.required),     });
            this.editForm.controls.sorting.setValue('defult')
            console.log( this.editForm.controls.sorting.setValue('defult'))
    }

    ionViewWillEnter() {
        
       
        this.categorySlug =   localStorage.getItem("Slug");
        console.log(this.categorySlug)
        //this.Getselected(selected_value);
        this.getProducts();
        
        this.tbarService.hideBar = false;
    }
    Getselected(selected_value)
    {
    console.log("selector: ", selected_value );
    }
    
    
    async getProducts() {
        console.log(this.sorting)
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
            var loadingModal = await this.loadingCtrl.create({
                spinner: null,
                message: "Please wait...",
                translucent: true,
                cssClass: "custom-class custom-loading",
            });
            loadingModal.present();
            this.storage.get('categorySlug').then((data) => {
                this.categorySlug = data
                
                console.log(this.categorySlug, this.page);
                if(this.categorySlug==null){
                   this.categorySlug=localStorage.getItem("Slug");
                }
                // alert( this.categorySlug);
            })
          
            this.wooService.getListProducts({ cat: this.categorySlug, page: this.page,orderby:this.sorting }).subscribe((response: any) => {

                loadingModal.dismiss();
                this.products = response.products;
               
                console.log("category::", JSON.stringify(this.products));
                if (this.products == undefined || !this.products) {
                    this.AlertConfirm()
                } else {

                    if (this.products.length < this.per_page) {
                        this.has_more = false;
                    }
                    else {
                        this.page++;
                    }
                }



            }, async (reason) => {
                loadingModal.dismiss();

                const toast = await this.toastController.create({
                    message: value["NetWork_Error"],
                    duration: 2000,
                    color: "dark",
                });
                toast.present();
            });
        });

    }

    doInfinite(infiniteScroll) {
        if (this.has_more) {
            this.translateService.get(['Notice', 'NetWork_Error', 'OK']).subscribe(value => {
                
                var categorySlug = localStorage.getItem("Slug");
                console.log("cat:", categorySlug,"page", this.page);
                this.wooService.getListProducts({ cat: categorySlug, page: this.page,orderby:this.sorting }).subscribe((response: any) => {
                    response.products.forEach(p => {
                        this.products.push(p);
                    });

                    if (response.products.length < this.per_page) {
                        this.has_more = false;
                    }
                    else {
                        this.page++;
                    }
                    infiniteScroll.target.complete();
                }, async (reason) => {
                    infiniteScroll.target.complete();
                    const toast = await this.toastController.create({
                        message: value["NetWork_Error"],
                        duration: 2000,
                        color: "dark",
                    });
                    toast.present();
                });
            });

        }
        else {
            infiniteScroll.target.complete();
            infiniteScroll.target.disabled = true;
        }
    }

    viewProduct(id) {
        console.log('id', id)
        this.navCtrl.navigateForward('details/' + id);
    }
   
    
    ionChange(){
        console.log(this.selected)
        this.sorting = this.selected
        this.categorySlug =   localStorage.getItem("Slug")
        this.getProducts()
    }


    doRefresh(refresher) {
        console.log( this.sorting)
        this.page = 1;
        this.translateService.get(['Notice', 'NetWork_Error', 'OK']).subscribe(value => {
            this.wooService.getListProducts({ cat: this.categorySlug ,orderby:this.sorting}).subscribe((response: any) => {
                this.products = response.products;
                if (response.products.length < this.per_page) {
                    this.has_more = false;
                }
                else {
                    this.page++;
                }
                refresher.target.complete();;
            }, async (reason) => {
                refresher.target.complete();;
                const toast = await this.toastController.create({
                    message: value["NetWork_Error"],
                    duration: 2000,
                    color: "dark",
                });
                toast.present();
            });
        });

    }
    openSidemenu() {
        this.menuCtrl.open();
    }



    async AlertConfirm() {
        const toast = await this.toastController.create({
            cssClass: 'my-custom-class',
            message: 'Oops! No Products Yet ',
            color: 'dark',
            buttons: [
                {
                    text: 'Ok',

                    cssClass: 'secondary',
                    handler: () => {
                       // this.router.navigate(['tabs/category'])
                    }

                }
            ]
        });

        toast.present();
    }
}
function refresher(refresher: any) {
    throw new Error('Function not implemented.');
}

