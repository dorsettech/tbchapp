import { Component, OnInit, ViewEncapsulation, Renderer } from '@angular/core';
import { NavController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { SowService } from '../../providers/sow-service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
/**
 * Generated class for the SearchVendor page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'app-page-search-vendor',
    templateUrl: 'search-vendor.html',
    styleUrls: ['search-vendor.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SearchVendorPage {
    myInput: string = '';
    noResult: boolean;
    loadingModal: any;
    vendors: any;
    constructor(public navCtrl: NavController, public sowService: SowService,
        public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translateService: TranslateService, public renderer: Renderer,
        public keyboard: Keyboard, public toastController: ToastController) {
    }

    ionViewDidLoad() {
    }

    myOnSearch(event) {
        this.renderer.invokeElementMethod(event.target, 'blur');
        this.keyboard.hide();
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
             var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
            this.sowService.searchVendor({ 'key_word': this.myInput }).subscribe((data) => {
                this.vendors = data;
                loadingModal.dismiss();
            }, (reson) => {
                loadingModal.dismiss();
                this.toastController.create({
                    // header: value['Notice'],
                    message: value["NetWork_Error"],
                    duration: 2000,
                    color: 'dark'
                });
            });
        });
    }

    onSearch() {
        this.renderer.invokeElementMethod(event.target, 'blur');
        this.keyboard.hide();
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
              var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
            this.sowService.searchVendor({ 'key_word': this.myInput }).subscribe((data) => {
                this.vendors = data;
                loadingModal.dismiss();
            }, (reson) => {
                loadingModal.dismiss();
                this.toastController.create({
                    // header: value['Notice'],
                    message: value["NetWork_Error"],
                    duration: 2000,
                    color: 'dark'
                });
            });
        });

    }


    vendorSelected(vendor) {
        this.navCtrl.navigateForward('store/vendor/' + vendor.id);
    }

}
