import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';
import { SearchVendorPage } from './search-vendor';

@NgModule({
  declarations: [
    SearchVendorPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: SearchVendorPage
      }
    ])
  ],
})
export class SearchVendorPageModule { }
