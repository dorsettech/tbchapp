/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:11:45
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, AlertController, MenuController, ToastController } from '@ionic/angular';
import { UserService } from '../../providers/user-service';
import { TbarService } from '../../providers/tbar-service';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { LoginPage } from '../login/login';
import { Router } from '@angular/router';
import { Events } from '../../providers/events'

/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'app-page-profile',
    templateUrl: 'profile.html',
    styleUrls: ['profile.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProfilePage {

    constructor(public navCtrl: NavController, public modalCtrl: ModalController, public userService: UserService, public events: Events, public alertCtrl: AlertController, public menuCtrl: MenuController, public tbarService: TbarService, public storage: Storage, public translateService: TranslateService,
        public router: Router, public toastController: ToastController) {

    }

    openOrderList() {
        this.navCtrl.navigateForward('orderlist');
        if (this.userService.isAuthenticated) {
            this.navCtrl.navigateForward('orderlist');
        } else {
            this.translateService.get(['Notice', 'No_Sign_In', 'OK']).subscribe(async value => {

                const toast = await this.toastController.create({
                    message: value["No_Sign_In"],
                    duration: 2000,
                    color: "dark",
                });
                toast.present();
            });

        }
    }

    ionViewWillEnter() {
        this.tbarService.hideBar = false;
    }



    async loginOut() {
        let alert = await this.alertCtrl.create({
            header: "Are you sure",
            message: "You want to remove this product from cart?",
            buttons: [
                {
                    text: "No",
                    handler: async () => {
                        console.log("Cancel clicked");

                        this.storage.remove("oddwolves-user-info");
                        this.menuCtrl.close();
                        this.userService.id = "";
                        this.userService.email = "";
                        this.userService.first_name = "";
                        this.userService.last_name = "";
                        this.userService.name = "";
                        this.userService.username = "";
                        this.userService.isAuthenticated = false;
                        const toast = await this.toastController.create({
                            message: "Successfully Logged Out",
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                        this.events.publish("logout", {});
                        this.navCtrl.navigateRoot(["/main"]);
                    },
                },
                {
                    text: "Yes",
                    handler: async () => {
                        this.storage.clear();
                        this.menuCtrl.close();
                        this.userService.id = "";
                        this.userService.email = "";
                        this.userService.first_name = "";
                        this.userService.last_name = "";
                        this.userService.name = "";
                        this.userService.username = "";
                        this.userService.isAuthenticated = false;
                        const toast = await this.toastController.create({
                            message: "Successfully Logged Out",
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                        this.events.publish("logout", {});
                        this.navCtrl.navigateRoot(["/main"]);
                    },
                },
            ],
        });

        await alert.present();
    }

    login() {

        this.modalCtrl.create({ component: LoginPage }).then((value) => {
            value.present();
        });

    }

    openEditAddress() {
        if (this.userService.isAuthenticated) {
            this.navCtrl.navigateForward('address');
        } else {
            this.translateService.get(['Notice', 'No_Sign_In', 'OK']).subscribe(async value => {

                const toast = await this.toastController.create({
                    message: value["No_Sign_In"],
                    duration: 2000,
                    color: "dark",
                });
                toast.present();
            });

        }
    }
    openSidemenu() {
        this.menuCtrl.open();
    }
    wishlist() {
        this.navCtrl.navigateForward('wishlist');
        if (this.userService.isAuthenticated) {
            this.navCtrl.navigateForward('wishlist');
        } else {
            this.translateService.get(['Notice', 'No_Sign_In', 'OK']).subscribe(async value => {

                const toast = await this.toastController.create({
                    message: value["No_Sign_In"],
                    duration: 2000,
                    color: "dark",
                });
                toast.present();
            });

        }
    }
}
