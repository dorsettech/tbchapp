import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController, ToastController } from '@ionic/angular';
import { UserService } from '../../providers/user-service';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Storage } from "@ionic/storage";
/*
  Generated class for the OrderList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-order-list',
    templateUrl: 'order-list.html',
    styleUrls: ['order-list.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OrderListPage {
    orderList: Array<any>;
    page: number;
    per_page: number;
    errorModal: any;
    has_more: boolean;
    loadingModal: any;
    previousPage: any;
    cust_id: any;
    constructor(public navCtrl: NavController, public userService: UserService, public wooService: WoocommerceService, public toastController: ToastController,
        public loadingCtrl: LoadingController, public menuCtrl: MenuController, public alertCtrl: AlertController, public translateService: TranslateService,
        public activatedRoute: ActivatedRoute, private router: Router,public storage:Storage) {
        this.orderList = new Array<any>();
        this.page = 1;
        this.per_page = 10;
        this.has_more = true;
       
                         

    }

    back() {
        this.navCtrl.back();
    }
    ionViewWillEnter(){
        this.storage.get("userService.id").then((val) => {
            //console.log("value", val);
            if (val) {
                //console.log(val);
                this.cust_id = val;
                console.log( this.cust_id)
                this.getlist()
               
            }
        });
    }
    ngOnInit() {
        
    }
   async getlist(){
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
         
            console.log('customer', this.cust_id);
         var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
            this.wooService.getOrderList({
                page: this.page, per_page: this.per_page,
                customer: this.cust_id

            }).subscribe((orderlist: Array<any>) => {
                this.orderList = orderlist;
                console.log(this.orderList)
                if (orderlist.length < this.per_page) {
                    this.has_more = false;
                }
                else {
                    this.page++;
                }
                loadingModal.dismiss();
            }, async (reason) => {
                loadingModal.dismiss();

                    const toast = await this.toastController.create({
                            message: "Oops! Network offline",
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
            });
        });

    }
    doRefresh(refresher) {
        this.page = 1;
        this.translateService.get(['Notice', 'NetWork_Error', 'OK']).subscribe(async value => {
            console.log(this.cust_id)
            this.wooService.getOrderList({
                page: this.page, per_page: this.per_page
                , customer: this.cust_id
            }).subscribe((orderlist: Array<any>) => {
                this.orderList = orderlist;
                if (orderlist.length < this.per_page) {
                    this.has_more = false;
                }
                else {
                    this.page++;
                }
                refresher.target.complete();;
            }, async (reason) => {
                refresher.target.complete();;
                const toast = await this.toastController.create({
                    message: 'Something Went wrong',
                    duration: 2000,
                    color: 'dark'
                });
                toast.present();
            });
        });

    }

    doInfinite(infiniteScroll) {
        if (this.has_more) {
            this.translateService.get(['Notice', 'NetWork_Error', 'OK']).subscribe(value => {
                console.log(this.cust_id)
                this.wooService.getOrderList({ page: this.page, per_page: this.per_page, customer: this.cust_id }).subscribe((orderlist: Array<any>) => {
                    orderlist.forEach(p => {
                        this.orderList.push(p);
                    });

                    if (orderlist.length < this.per_page) {
                        this.has_more = false;
                    }
                    else {
                        this.page++;
                    }
                    infiniteScroll.target.complete();
                }, async (reason) => {
                    infiniteScroll.target.complete();
                    const toast = await this.toastController.create({
                        message: 'Something Went wrong',
                        duration: 2000,
                        color: 'dark'
                    });
                    toast.present();
                });
            });
        }
        else {
            infiniteScroll.target.complete();
            infiniteScroll.target.disabled = true;
        }
    }

    viewOrder(oid) {
        this.navCtrl.navigateForward('tabs/my/orderlist/orderdetails/' + oid);
    }
    openSidemenu() {
        this.menuCtrl.open();
    }
}
