import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { OrderListPage } from './order-list';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    OrderListPage
  ],
  imports: [
    IonicModule,CommonModule,
    FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: OrderListPage
      }
    ])
  ],
})
export class OrderListPageModule { }
