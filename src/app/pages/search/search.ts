/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 19/06/2021 - 10:40:14
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 19/06/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { TranslateService } from '@ngx-translate/core';
/*
  Generated class for the Search page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'app-page-search',
    templateUrl: 'search.html',
    styleUrls: ['search.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SearchPage {
    myInput: string = '';
    products: any;
    noResult: boolean;
    errorModal: any;
    constructor(public navCtrl: NavController, public viewCtrl: ModalController, public wooService: WoocommerceService,
        public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translateService: TranslateService, public toastController: ToastController) {
        this.noResult = false;

    }

    onSearch() {
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
            var loadingModal = await this.loadingCtrl.create({
                spinner: null,
                message: "Please wait...",
                translucent: true,
                cssClass: "custom-class custom-loading",
            });
            loadingModal.present();
            this.wooService.getProducts({ search: this.myInput }, 'product').subscribe((products: Array<any>) => {
                if (products.length == 0) {
                    this.noResult = true;
                } else {
                    this.products = products;
                }
                loadingModal.dismiss();
            }, async (reason) => {
                loadingModal.dismiss();

                const toast = await this.toastController.create({
                    message: value["NetWork_Error"],
                    duration: 2000,
                    color: 'dark'
                });
                toast.present();
            });
        });

    }
    onClose() {
        this.viewCtrl.dismiss();
    }

    viewProduct(id) {
        this.navCtrl.navigateForward('details/' + id);
    }
}
