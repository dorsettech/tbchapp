/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:11:23
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import {
    Component,
    ViewChild,
    ElementRef,
    OnInit,
    ViewEncapsulation,
} from "@angular/core";
import {
    NavController,
    LoadingController,
    AlertController,
    PopoverController,
    ModalController,
    MenuController,
} from "@ionic/angular";
import {
    WoocommerceService,
    CartProduct,
    WishlistProduct,
} from "../../providers/woocommerce-service";
import { TbarService } from "../../providers/tbar-service";
import { Storage } from "@ionic/storage";
import { AppConfig } from "../../app-config";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute } from "@angular/router";
import { AttributePage } from "../attribute/attribute";
import { Router, Params } from "@angular/router";
import { ToastController } from "@ionic/angular";
import { text } from "@angular/core/src/render3";
import { HttpClient } from '@angular/common/http';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

/*
  Generated class for the ProductDetails page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
import * as $ from "jquery";
@Component({
    selector: "app-page-product-details",
    templateUrl: "product-details.html",
    styleUrls: ["product-details.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class ProductDetailsPage {
    @ViewChild("currencyFormat") currencySymbol: ElementRef;
    product: any;
    loadingModal: any;
    selVariation: any;
    mySlideOptions: any;
    hasVariation: boolean = false;
    slideImages: Array<any>;
    variationImg: string;
    show = true;
    errorModal: any;
    cartProductNumber = 0;
    isFav = false;
    org: string;
    attrArray: any;
    id: string;
    from: string;
    showShortDesciption = true
    cartLength: Promise<any>;
    cart: any;
    blankDesc = '<p></p>';
    showDesc: boolean = false;
    is_product_addons: boolean = false;
    product_addons_data :any=[];
    product_addons_question :any={};
    product_addons_required :any=[];
    product_addons_meta:any={};
    get_images :any='';
    get_images_array:any={};
    img:any='';
    allCheckbox:any = {};
    constructor(
        public navCtrl: NavController,
        public wooService: WoocommerceService,
        public loadingCtrl: LoadingController,
        public tbarService: TbarService,
        public storage: Storage,
        public alertCtrl: AlertController,
        public appConfig: AppConfig,
        public popoverCtrl: PopoverController,
        public modalCtrl: ModalController,
        public translateService: TranslateService,
        private activatedRoute: ActivatedRoute,
        public router: Router,
        public toastController: ToastController,
        public menuCtrl: MenuController,
        private socialSharing: SocialSharing,
        public httpClient:HttpClient,
        private camera: Camera
    ) {
        this.id = this.activatedRoute.snapshot.paramMap.get("id");
        console.log(this.id)
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.from = params["from"];
            console.log(this.from)
        });

        this.selVariation = null;
        this.slideImages = new Array<any>();
        this.mySlideOptions = {
            pager: true,
            loop: true,
            autoplay: 2000,
        };
    }

    ionViewWillEnter() {

        this.storage.get("oddwolves-cart").then((cartLength) => {
            console.log(cartLength)
            this.cart = JSON.parse(cartLength);
            if (this.cart) {
                this.cartProductNumber = this.cart.length;
                console.log(this.cartProductNumber)
            } else {
                this.cartProductNumber = 0;
            }

        });

    }
    desc() {
        if ($("#desc").find("p").text().length > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    openSidemenu() {
        this.menuCtrl.open();
    }

    ngOnInit() {
        this.translateService
            .get(["Notice", "Loading", "NetWork_Error", "OK"])
            .subscribe((value) => {
                var loading = null;
                this.loadingCtrl
                    .create({
                        message: value["Loading"],
                    })
                    .then((value) => {
                        loading = value;
                        loading.present();
                    });
                console.log(this.id)
                this.wooService.getSingleProduct(this.id).subscribe(
                    (product: any) => {
                        this.product = product;
                        console.log("products---", this.product,)

                        this.product.description = $(this.product.description).text();
                        // this.product.description =  ? '' : this.product.description; 
                        this.attrArray = product.attributes;
                        this.slideImages = this.product.images;
                        if (this.product.variations.length > 0) {
                            this.hasVariation = true;
                        }
                        this.storage.get("oddwolves-wishlist").then((data) => {
                            if (data) {
                                var wishlistArray = JSON.parse(data);
                                var findIndex = wishlistArray.findIndex((element) => {
                                    return element.product_id == this.product.id;
                                });
                                if (findIndex != -1) {
                                    this.isFav = true;
                                }
                            }
                        });

                        this.product.meta_data.map((item,index)=>{
                            if(item.key == '_product_addons'){
                                this.is_product_addons = true;
                                for(let i = 0; i<=item.value.length -1; i++){
                                    
                                    this.product_addons_data = item.value;
                                    
                                }
                                console.log("+++")
                                console.log(this.product_addons_data)
                                console.log("+++")  
                            }
                        })

                        this.product_addons_data.map((item,index)=>{
                           // console.log(item.display)
                            if(item.display == 'images'){
                              
                               item.options.map((data,index)=>{
                                let url = '/wp-json/wp/v2/get_image?id='+data.image+'&size=thumbnails';
                                this.wooService.get(url).subscribe(response => {    
                                   this.get_images = response;
                                   if(this.get_images.status == 'success'){
                                        this.get_images_array[data.image] = this.get_images.data.url;
                                   }
                               });
                               })
                               
                           }
                        })

                        console.log(this.get_images_array)

                        loading.dismiss();
                    },
                    async (reason) => {
                        loading.dismiss();
                        const toast = await this.toastController.create({
                            message: "NetWork_Error",
                            duration: 2000,
                            color: 'dark'
                        });
                        toast.present();
                    }
                );
            });
    }
    async errorMsg(msg){
                        const toast = await this.toastController.create({
                            message: msg,
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
    }

     getAttachmentImage(imageid){
        let url = '/wp-json/wp/v2/get_image?id='+imageid+'&size=thumbnails';
       
             this.wooService.get(url).subscribe(response => {    
                this.get_images = response;
                if(this.get_images.status == 'success'){
                    debugger;
                    return this.get_images.data.url;
                }
            });

        
      
    }

    checkboxData(name,label){
        if(name in this.allCheckbox){
            if(this.allCheckbox[name].indexOf(label) !== -1)  
                {  
                    const index = this.allCheckbox[name].indexOf(label); 
                    this.allCheckbox[name].splice(index, 1);
                }   
                else  
                {  
                    this.allCheckbox[name].push(label);
                }   
        }else{
            this.allCheckbox[name] = [label];
        }
            console.log(this.allCheckbox);
    }

    addCartClip() {
       
        console.log(this.product_addons_question);
        console.log(this.allCheckbox);
        var is_error =0;   
        this.product_addons_data.map(async(item,index)=>{
            if(item.type != 'checkbox'){
                if(item.required == 1 && this.product_addons_question[item.name] == undefined){
                    this.errorMsg('All * fileds are mandatory');
                    is_error = 1;
                }else if(item.required == 1 && this.product_addons_question[item.name] != undefined){
                    is_error = 0;   
                }
            } 
        })

        this.product_addons_data.map(async(item,index)=>{
           
             if(item.required == 1 && item.type == 'checkbox'){
                 if(this.allCheckbox[item.name] == undefined){
                    this.errorMsg('All * fileds are mandatory');
                    is_error = 1;
                 }else if(this.allCheckbox[item.name].length < 1){
                    this.errorMsg('All * fileds are mandatory');
                    is_error = 1;
                 }
             }
        })

       if(is_error == 0){
        this.translateService.get(["Notice", "Product_added_successfully", "Please_select_variation", "OK",]).subscribe(async (value) => {
            this.selVariation = this.getSelectedVariation();
            if (this.selVariation != null || this.hasVariation == false) {
                var cartArray: Array<CartProduct> = new Array<CartProduct>();
                this.storage.get("oddwolves-cart").then(async (data) => {
                    console.log(data)
                    if (data && data.length) {
                        cartArray = JSON.parse(data);
                        if (this.hasVariation == true) {
                            var findIndex = cartArray.findIndex((element) => {
                                console.log("findIndex--", findIndex)
                                return (
                                    element.product_id == this.product.id &&
                                    element.variation_id == this.selVariation.id
                                );
                            });

                            if (findIndex != -1) {
                                cartArray[findIndex].quantity = Number(cartArray[findIndex].quantity) + 1;
                                this.storage.set('oddwolves-cart', JSON.stringify(cartArray));
                                const toast = await this.toastController.create({
                                    message: value["Product_added_successfully"],
                                    duration: 2000,
                                    color: 'dark'
                                });
                                toast.present();
                            } else {
                                if (cartArray[0].vendor_id == this.product.store.vendor_id) {
                                    cartArray.push(this.createNewProduct());
                                } else {
                                    var alert = await this.alertCtrl.create({
                                        // header: value["Notice"],
                                        message: "We noticed you have products from another seller in your basket. Please visit your basket and checkout with their items first and then come back for this one. Thanks so much",
                                        buttons: [value["OK"]],
                                    });
                                    alert.present();
                                    return false;
                                }
                            }

                            const toast = await this.toastController.create({
                                message: value["Product_added_successfully"],
                                duration: 2000,
                                color: "dark",
                            });
                            toast.present();
                            this.storage.set("oddwolves-cart", JSON.stringify(cartArray));
                        } else {
                            console.log('cartArray:: ', cartArray);
                            var findIndex = cartArray.findIndex((element) => {
                                console.log("xyz--", element)
                                return element.product_id == this.product.id;
                            });
                            console.log("xyz--", findIndex)
                            if (findIndex != -1) {
                                console.log("here")
                                cartArray[findIndex].quantity = Number(cartArray[findIndex].quantity) + 1;
                                this.storage.set('oddwolves-cart', JSON.stringify(cartArray));
                                const toast = await this.toastController.create({
                                    // header: value["Notice"],
                                    message: value["Product_added_successfully"],
                                    buttons: [value["OK"]],
                                });
                                toast.present();
                                return false;
                            } else {
                                console.log("here2")
                                console.log('cartArray:: ', cartArray.length);
                                if (cartArray.length) {
                                    if (cartArray[0].vendor_id == this.product.store.vendor_id) {
                                        console.log(cartArray[0].vendor_id, this.product.store.vendor_id)
                                        cartArray.push(this.createNewProduct());
                                    } else {


                                        const toast = await this.toastController.create({
                                            message: "We noticed you have products from another seller in your basket. Please visit your basket and checkout with their items first and then come back for this one. Thanks so much",
                                            duration: 2000,
                                            color: "danger",
                                        });
                                        toast.present();
                                        return false;
                                    }
                                } else {
                                    cartArray.push(this.createNewProduct());
                                    const toast = await this.toastController.create({
                                        message: value["Product_added_successfully"],
                                        duration: 2000,
                                        color: "dark",
                                    });
                                    toast.present();
                                    this.storage.set("oddwolves-cart", JSON.stringify(cartArray));
                                }



                            }

                            const toast = await this.toastController.create({
                                message: value["Product_added_successfully"],
                                duration: 2000,
                                color: "dark",
                            });
                            toast.present();
                            this.storage.set("oddwolves-cart", JSON.stringify(cartArray));
                        }
                    } else {
                        cartArray.push(this.createNewProduct());
                        const toast = await this.toastController.create({
                            message: value["Product_added_successfully"],
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                        this.storage.set("oddwolves-cart", JSON.stringify(cartArray));
                    }

                    this.tbarService.cartBage = cartArray.length;
                    this.cartProductNumber = cartArray.length;
                });
            } else {
                this.alertCtrl
                    .create({
                        // header: value["Notice"],
                        message: value["Please_select_variation"],
                        buttons: [value["OK"]],
                    })
                    .then((value) => {
                        value.present();
                    });
            }
        });
        }
    }


    createNewProduct() {
        var newProduct = new CartProduct();
        console.log('this.product:: ', this.product);
        newProduct.product_id = this.product.id;
        newProduct.name = this.product.name;
        newProduct.quantity = 1;
        newProduct.vendor_id = this.product.store.vendor_id;
        newProduct.vendor_name = this.product.store.vendor_shop_name;
        newProduct.addons_data        = this.product_addons_data;
        newProduct.addons_data_select = this.product_addons_question;
        newProduct.addons_data_checkbox = this.allCheckbox;
        if (this.selVariation != null) {
            newProduct.variation_id = this.selVariation.id;
            newProduct.price = this.selVariation.price;
            if (this.selVariation.image && this.selVariation.image[0]) {
                newProduct.thumb = this.selVariation.image[0].src;
            } else {
                newProduct.thumb = this.product.images[0].src;
            }
            var temp_variation = "";
            this.selVariation.attributes.forEach((attr) => {
                newProduct.variation_name += attr.option + " ";
                temp_variation += '"' + attr.name + '":"' + attr.option + '",';
            });

            if (temp_variation.length > 0) {
                temp_variation = temp_variation.substr(0, temp_variation.length - 1);
            }
            temp_variation = "{" + temp_variation + "}";
            var temp_variation_json = JSON.parse(temp_variation);

            newProduct.variations = temp_variation_json;
        } else {
            newProduct.variation_id = 0;
            newProduct.price = this.product.price;
            newProduct.thumb = this.product.images[0].src;
        }
        return newProduct;
    }

    back() {
        this.navCtrl.back();
    }

    goToCart() {
        if (this.from == "cart") {
            this.navCtrl.back();
        } else {
            this.navCtrl.navigateForward("cart");
        }
    }
   

    addWishlist() {
        this.translateService
            .get(["Removed_from_wishlist", "Added_to_wishlist"])
            .subscribe((value) => {
                if (this.isFav == true) {
                    this.storage.get("oddwolves-wishlist").then(async (data) => {
                        if (data) {
                            var wishlistArray: Array<WishlistProduct> = JSON.parse(data);
                            var findIndex = wishlistArray.findIndex((element) => {
                                return element.product_id == this.product.id;
                            });
                            if (findIndex != -1) {
                                wishlistArray.splice(findIndex, 1);
                                this.storage.set(
                                    "oddwolves-wishlist",
                                    JSON.stringify(wishlistArray)
                                ); console.log(wishlistArray)
                                const toast = await this.toastController.create({
                                    message: value["Removed_from_wishlist"],
                                    duration: 2000,
                                    color: "dark",
                                });
                                toast.present();
                            }
                        }
                    });
                    this.isFav = false;
                } else {
                    this.storage.get("oddwolves-wishlist").then(async (data) => {
                        var wishlistArray: Array<WishlistProduct> = new Array<
                            WishlistProduct
                        >();
                        if (data) {
                            wishlistArray = JSON.parse(data);
                        }
                        const toast = await this.toastController.create({
                            message: value["Added_to_wishlist"],
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                        var wp = new WishlistProduct();
                        wp.price = this.product.price;
                        console.log(wp.price)
                        wp.product_id = this.product.id;
                        console.log(wp.product_id)
                        wp.product_image = this.product.images[0].src;
                        console.log(wp.product_image)
                        wishlistArray.push(wp);
                        this.storage.set(
                            "oddwolves-wishlist",
                            JSON.stringify(wishlistArray)
                        );
                        console.log(wishlistArray)
                    });
                    this.isFav = true;
                }
            });
    }

    share() {

        this.translateService
            .get([
                "Notice",
                "Loading",
                "NetWork_Error",
                "OK",
                "Share_Success",
                "Share_Fail",
            ])
            .subscribe(async (value) => {
                var loading = await this.loadingCtrl.create({
                    message: value["Loading"],
                });
                loading.present();
                this.socialSharing.share(
                    "Sell " +
                    this.product.name +
                    " for only " +
                    this.product.price +
                    this.currencySymbol.nativeElement.innerHTML +
                    ", for more details check this out.",
                    this.product.name,
                    null,
                    this.product.permalink
                )
                    .then(async () => {
                        console.log("here")
                        loading.dismiss();


                    })
                    .catch(async (e) => {
                        console.log("here2", e)
                        loading.dismiss();

                    });
            });
    }

    async attributeSelected(item) {
        var changeAttrOption = (<Array<any>>this.product.attributes).find(
            (value) => {
                return value.name == item.name;
            }
        );
        if (changeAttrOption) {
            let popover = await this.popoverCtrl.create({
                component: AttributePage,
                componentProps: {
                    options: item.options,
                    attr: item.name,
                    option: changeAttrOption
                        ? changeAttrOption.option
                            ? changeAttrOption.option
                            : ""
                        : "",
                },
            });
            popover.onDidDismiss().then((data) => {
                if (data.data) {
                    (<Array<any>>this.product.attributes).forEach((element) => {
                        if (element.name == data.data.attr) {
                            element.option = data.data.option;
                            return;
                        }
                    });
                    this.getSelectedVariation();
                }
            });
            popover.present();
        }
    }
    uploadFile(name){
        const options: CameraOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
          }
          
          this.camera.getPicture(options).then((data) => {
                      const base64Image = 'data:image/jpeg;base64,' + data;
                      this.img = base64Image;
                      this.product_addons_question[name] = this.img; 
                       console.log(base64Image) 
                     // this.AddStoryPagePost();
                      
              
             
            },(err) => {
           // Handle error
          });
    }

    getSelectedOption(attr) {
        var changeAttrOption = (<Array<any>>this.product.attributes).find(
            (value) => {
                return value.name == attr;
            }
        );
        return changeAttrOption
            ? changeAttrOption.option
                ? changeAttrOption.option
                : ""
            : "";
    }

    getSelectedVariation() {
        var allSelected = true;
        (<Array<any>>this.product.attributes).forEach((element) => {
            if (!element.option) {
                allSelected = false;
                return;
            }
        });

        if (!allSelected) {
            this.selVariation = null;
        }

        var selectedVariation = (<Array<any>>this.product.variations).find(
            (value) => {
                var variation_attr_array = value.attributes;
                var len = (<Array<any>>variation_attr_array).length;
                var same = true;
                for (var i = 0; i < len; i++) {
                    if (
                        (<Array<any>>variation_attr_array)[i].option !=
                        (<Array<any>>this.product.attributes)[i].option
                    ) {
                        same = false;
                        break;
                    }
                }
                return same;
            }
        );
        if (selectedVariation) {
            this.selVariation = selectedVariation;
            this.variationImg = selectedVariation.image[0].src;

            return selectedVariation;
        }
        return null;
    }
    alterDescriptionText() {
        this.showShortDesciption = !this.showShortDesciption
    }

    shopname(id) {
        console.log("Id", id)
        this.navCtrl.navigateForward(["shop-products"], { queryParams: { id } });
    }
    getPrice(text,price){

        return text+' +£'+price;
    }
}




