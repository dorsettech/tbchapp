/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:13:03
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation, ViewChild } from "@angular/core";
import {
    NavController,
    LoadingController,
    AlertController,
    ToastController,
} from "@ionic/angular";
import { WoocommerceService } from "../../providers/woocommerce-service";
import {
    IonSlides,
    IonSlide,
    IonContent,
    MenuController,
} from "@ionic/angular";
import { TbarService } from "../../providers/tbar-service";
import { SowService } from "../../providers/sow-service";
import { AppConfig } from "../../app-config";
import { TranslateService } from "@ngx-translate/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
// import * as $ from "jquery";

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
declare var window: any;
declare var select2: any;
declare var $: any;
@Component({
    selector: "app-shop-products",
    templateUrl: "shop-products.page.html",
    styleUrls: ["shop-products.page.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class ShopProductsPage implements OnInit {
    counties: any;
    page: number;
    per_page: number;
    has_more: boolean;
    loadingModal: any;
    errorModal: any;
    slides: Array<any>;
    load_slide_end: boolean;
    load_products_end: boolean;

    showSlide = false;
    categories: any = [];
    products: Array<any> = new Array<any>();
    searchProducts: Array<any> = new Array<any>();
    searchInput: any = "";
    noSearchResult: boolean = false;
    vendorId: any = '';
    temp_token: any;
    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public wooService: WoocommerceService,
        private router: Router,
        public tb: TbarService,
        public alertCtrl: AlertController,
        public sowService: SowService,
        public appConfig: AppConfig,
        public translateService: TranslateService,
        public menuCtrl: MenuController,
        public toastController: ToastController,
        private activatedRoute: ActivatedRoute,
        private storage:Storage
    ) {
        let that = this;
        window.addEventListener("keyboardWillHide", () => {
            that.searchProducts = [];
        });
       
    }
    @ViewChild("itemSlide") categorySlide: IonSlides;
    slideOpts = {
        slidesPerView: 3.2,
        autoplay: false,
    };

    openSidemenu() {
        this.menuCtrl.open();
    }
onSearch(event){
  
}
    ionViewWillEnter() {
        this.page = 1;
        this.per_page = 10;
        this.has_more = true;

        this.load_products_end = false;
        this.load_slide_end = false;
        // this.getProductsAndSlider();
        this.activatedRoute.queryParams.subscribe(params => {
            if (params) {
                this.vendorId = (params.id);
                this.getShopProductList(this.vendorId);
            }
        });
    }

    doRefresh(refresher) {
        this.page = 1;
        this.translateService
            .get(["Notice", "NetWork_Error", "OK"])
            .subscribe((value) => {
                this.wooService
                    .shopProductList({ vendor_id: this.vendorId,page: this.page })
                    .subscribe(
                        (products: Array<any>) => {
                            console.log("Products: ", JSON.stringify(products));
                            this.products = products;

                            if (!products.length) {
                                this.AlertConfirm()
                            } else {

                                if (this.products.length < this.per_page) {
                                    this.has_more = false;
                                }
                                else {
                                    this.page++;
                                }
                            }
                            refresher.target.complete();
                        },
                        (reson) => {
                            refresher.target.complete();
                            this.toastController.create({
                                // header: value["Notice"],
                                message: value["Network_Error"],
                                duration: 2000,
                                color: "dark",
                            });
                        }
                    );
            });
    }
    async AlertConfirm() {
        const toast = await this.toastController.create({
            cssClass: 'my-custom-class',
            message: 'Oops! No Products Yet ',
            color: 'dark',
            buttons: [
                {
                    text: 'Ok',

                    cssClass: 'secondary',
                    handler: () => {
                        this.router.navigate(['/tabs/home'])
                    }

                }
            ]
        });

        toast.present();
    }
    getShopProductList(vendor_id) {
        console.log(vendor_id)
        console.log("this.activatedRoute.snapshot.paramMap.get('id'):: ", this.vendorId);
        this.translateService
            .get(["Notice", "Loading", "NetWork_Error", "OK"])
            .subscribe(async (value) => {
                var loadingModal = await this.loadingCtrl.create({
                    message: value["Loading"],
                    keyboardClose: true,
                });
                this.wooService.shopProductList({ vendor_id: vendor_id, page: this.page }).subscribe(
                    (products: Array<any>) => {
                        //console.log("Shops: ", JSON.stringify(products));
                        console.log("Shops:",products)
                        this.products = products;
                        if (!products.length) {
                            this.AlertConfirm()
                        } else {

                            if (this.products.length < this.per_page) {
                                this.has_more = false;
                            }
                            else {
                                this.page++;
                            }
                        }
                        this.load_products_end = true;
                        loadingModal.dismiss();
                    },
                    async (reason) => {
                        loadingModal.dismiss();
                        const toast = await this.toastController.create({
                            message: value["Network_Error"],
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                    }
                );
            });
    }

    clearSearch() {
        this.searchInput = "";
        this.searchProducts = [];
    }

    viewProduct(id) {
        this.clearSearch();
        this.navCtrl.navigateForward("details/" + id);
        console.log('11111', id)
    }

    doInfinite(infiniteScroll) {
        this.translateService
            .get(["Notice", "NetWork_Error", "OK"])
            .subscribe((value) => {
                if (this.has_more) {
                    //   this.wooService
                    //     .getProducts({ page: this.page, per_page: this.per_page })
                    this.wooService.shopProductList({ vendor_id: this.vendorId, page: this.page }).subscribe(
                        (products: any) => {
                            products.forEach((p) => {
                                this.products.push(p);
                            });

                            if (products.length < this.per_page) {
                                this.has_more = false;
                            } else {
                                this.page++;
                            }
                            infiniteScroll.target.complete();
                        },
                        async (reason) => {
                            infiniteScroll.target.complete();
                            const toast = await this.toastController.create({
                                message: value["Network_Error"],
                                duration: 2000,
                                color: "dark",
                            });
                            toast.present();
                        }
                    );
                } else {
                    infiniteScroll.target.complete();
                    infiniteScroll.target.disabled = true;
                }
            });
    }




    OnInit() {
        this.tb.hideBar = false;
    }

    ngOnInit() { }


}
