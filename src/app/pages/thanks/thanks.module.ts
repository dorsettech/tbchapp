import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ThanksPage } from './thanks';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ThanksPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: ThanksPage
      }
    ])
  ],
})
export class ThanksPageModule { }
