/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:14:19
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import {
    NavController,
    NavParams,
    LoadingController,
    ModalController,
    AlertController,
} from "@ionic/angular";
import { WoocommerceService } from "../../providers/woocommerce-service";
import { UserService } from "../../providers/user-service";
import { TbarService } from "../../providers/tbar-service";
import { Storage } from "@ionic/storage";
import { AppConfig } from "../../app-config";
import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute, Router } from "@angular/router";
/*
  Generated class for the Thanks page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: "app-page-thanks",
    templateUrl: "thanks.html",
    styleUrls: ["thanks.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class ThanksPage {
    id: string;
    total: string;
    pay_id: string;
    ship_method: number;
    errorModal: any;
    pay_type: string;
    billing_addr: string;
    shipping_addr: string;
    isShow: boolean = false;
    order_id: any;
    constructor(
        public navCtrl: NavController,
        public storage: Storage,
        public navParams: NavParams,
        private router: Router,
        public loadingCtrl: LoadingController,
        public wooService: WoocommerceService,
        public userService: UserService,
        public appConfig: AppConfig,
        public viewCtrl: ModalController,
        public tbarService: TbarService,
        public alertCtrl: AlertController,
        public translateService: TranslateService,
        public modal:ModalController,
        private activatedRoute: ActivatedRoute
    ) {
       
        console.log(this.navParams.data)
        this.order_id = this.navParams.get("order_id");
        console.log( this.order_id)
    }
    ionViewWillEnter() {
     
        this.modal.dismiss();
        //this.getorder();
    }


    goHome() {
        this.router.navigateByUrl("");
        this.viewCtrl.dismiss();
    }
}
