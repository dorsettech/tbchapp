import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { WishlistPage } from './wishlist';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    WishlistPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: WishlistPage
      }
    ])
  ],
})
export class WishlistPageModule { }
