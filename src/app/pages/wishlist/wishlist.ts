import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, LoadingController, AlertController, ToastController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AppConfig } from '../../app-config';
import { TbarService } from '../../providers/tbar-service';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
/*
  Generated class for the Wishlist page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: 'app-page-wishlist',
    templateUrl: 'wishlist.html',
    styleUrls: ['wishlist.scss'],
    encapsulation: ViewEncapsulation.None
})
export class WishlistPage {
    products: any;
    wishlistProduct: any;
    loadingModal: any;
    isEmpty: boolean;
    errorModal: any;
    constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public modalCtrl: ModalController, public wooService: WoocommerceService,
        public storage: Storage, public appConfig: AppConfig, public tbarService: TbarService, public toastController: ToastController, public alertCtrl: AlertController,
        public translateService: TranslateService, private router: Router, public menuCtrl: MenuController,) {
        this.isEmpty = false;
    }

    ionViewDidEnter() {
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
            this.tbarService.hideBar = false;

            var loading = await this.loadingCtrl.create({
                message: value['Loading']
            });
            loading.present();

            this.storage.get('oddwolves-wishlist').then((data) => {
                if (data) {
                    this.wishlistProduct = JSON.parse(data);
                    if (this.wishlistProduct.length > 0) {
                        var includeID = '';
                        this.wishlistProduct.forEach(element => {
                            includeID += element.product_id + ',';
                        });
                        if (includeID.length > 0) {
                            includeID = includeID.substr(0, includeID.length - 1);
                        }

                        this.wooService.getProducts({ include: includeID },'product').subscribe((products: Array<any>) => {
                            this.products = products;
                            this.isEmpty = false;
                            loading.dismiss();
                        }, async (reason) => {
                            loading.dismiss();
                            const toast = await this.toastController.create({
                                message: value["NetWork_Error"],
                                duration: 2000,
                                color: 'dark'
                            });
                            toast.present();
                        });
                    }
                    else {
                        this.isEmpty = true;
                        loading.dismiss();
                    }
                } else {
                    this.isEmpty = true;
                    loading.dismiss();
                }
            });
        });

    }

    viewProduct(pid) {
        this.navCtrl.navigateForward('details/' + pid);
    }

    goHome() {
        this.router.navigateByUrl('');
    }

    deleteProduct(product) {
        event.stopPropagation();
        var findIndex = this.products.findIndex((element) => {
            return element.id == product.id;
        });
        if (findIndex != -1) {

            this.products.splice(findIndex, 1);
            if (this.products.length == 0) {
                this.isEmpty = true;
            }
        }
        findIndex = this.wishlistProduct.findIndex((element) => {
            return element.product_id == product.id;
        });
        this.wishlistProduct.splice(findIndex, 1);
        this.storage.set('oddwolves-wishlist', JSON.stringify(this.wishlistProduct));
    }
    openSidemenu() {
        this.menuCtrl.open();
    }
}
