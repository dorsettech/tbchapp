/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 26/04/2021 - 11:36:44
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 26/04/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import {
    NavController,
    NavParams,
    AlertController,
    LoadingController,
    ModalController,
    ToastController,
    IonSlides,
} from "@ionic/angular";
import { map, catchError, timeout } from "rxjs/operators";
import { Http, Headers } from "@angular/http";
//import { AppConfig } from '../../app-config';
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";
import { WoocommerceService } from "src/app/providers/woocommerce-service";
import { TbarService } from "../../providers/tbar-service";
import { Storage } from "@ionic/storage";
import { AppConfig } from "../../app-config";
import { UserService } from "src/app/providers/user-service";
import * as $ from "jquery";
import { ppid } from "process";
import { Router } from "@angular/router";
//import { log } from 'console';

declare var Stripe: any;
// declare var $: any;

/*
  Generated class for the StripePay page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: "app-page-stripe-pay",
    templateUrl: "stripe-pay.html",
    styleUrls: ["stripe-pay.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class StripePayPage {
    slideOptions = {
        initialSlide: 1,
        speed: 200,
    };
    autoplay: any;
    cardDetail: any = [];
    errorShow: boolean = false;
    isButton: boolean = false;

    order_data: any = {};
    // stripe = Stripe("pk_test_FvWubPv9FF6hXHFuTvDVgd2Y00s6f1ONkq");
    stripe: any;
    card: any;
    cardToken = "";
    sourceToken = '';
    makePayment: any;
    cardAmount: any;
    loginToken: any;
    //total = 0;
    desc = "";
    sameAddress: any;
    shippingAddress: any;
    billingAddress: any;
    selShipMethod: any;
    id: string;
    total: string;
    pay_id: string;
    ship_method: number;
    errorModal: any;
    pay_type: string;

    isShow: boolean = false;
    amount: any;
    result: true;
    paid: false;
    order_id: any;
    cust_id: any;
    stripeKey: any;
    product_amount: any;
    shipping_amount:any;
    shipping_id:any;
    shippingCost:any;
    totalPrice:any;
    isLoading :any;
    constructor(
        public loadingCtrl: LoadingController,
        public storage: Storage,
        public userService: UserService,
        public tbarService: TbarService,
        public alertCtrl: AlertController,
        public modalController: ModalController,
        public router: Router,
        public translateService: TranslateService,
        public modalCtrl: ModalController,
        public toastController: ToastController,
        public appConfig: AppConfig,
        public navParams: NavParams,
        public wooService: WoocommerceService,
        public navCtrl: NavController,
    ) {
        console.log("///////++++//////");
        console.log(this.navParams);

        console.log("///////++++//////");
        this.amount = this.navParams.data.total;
        this.pay_type = this.navParams.get("type");
        this.product_amount  = parseFloat(this.navParams.data.total) -  parseFloat(this.navParams.data.shipping);
        this.shipping_amount = parseFloat(this.navParams.data.shipping);
        this.shipping_id     = this.navParams.data.shipping_id
        this.shippingCost    = this.navParams.data.shipping;
        this.totalPrice      =  parseFloat(this.navParams.data.total);
        this.billingAddress = this.navParams.data.billingAddress;
        this.shippingAddress = this.navParams.data.shippingAddress;
        this.selShipMethod = this.navParams.data.ship_method;
        this.order_data = this.navParams.data.order_data;

        this.id = "";
        this.total = "";
        this.storage.get("stripeKey").then((val) => {
            if (val) {
                this.stripeKey = val;
                this.stripe = Stripe(
                    this.stripeKey
                );
            } else {
                console.log("error")
            }
        });
    }
    ionViewDidEnter() {
        this.translateService
            .get(["Notice", "Loading", "NetWork_Error", "OK"])
            .subscribe(async (value) => {
                if (
                    this.pay_type == "paypal" ||
                    this.pay_type == "stripe" ||
                    this.pay_type == "cod"
                ) {
                    var shippingAddress;
                    var billingAddress;
                    if (this.pay_type == "cod") {
                        shippingAddress = this.shippingAddress;
                        billingAddress = this.billingAddress;
                    }

                    var line_items = new Array();

                    this.storage
                        .get("oddwolves-user-billing-address")
                        .then((data) => {
                            if (data) {
                                billingAddress = JSON.parse(data);
                            }
                        })
                        .then(() => {
                            this.storage
                                .get("oddwolves-user-shipping-address")
                                .then((data) => {
                                    if (data) {
                                        shippingAddress = JSON.parse(data);
                                    }
                                })
                                .then(() => {
                                   
                                    this.storage.get("oddwolves-cart").then((data) => {
                                        this.storage.get("total").then((res) =>  {
                                            var total = res
                                        var cartProductArray = JSON.parse(data);
                                        var cartData = localStorage.getItem('oddwolves-cart');
                                        if(cartData.length > 0){
                                            var cartProductArray = JSON.parse(cartData);
                                        }
                                        console.log('cartProductArray');
                                        console.log(cartProductArray);
                                        console.log('cartProductArray');
                                        cartProductArray.forEach((element) => {
                                            var addonsMetaData= []
                                          
                                            
                                            line_items.push({
                                                product_id: element.product_id,
                                                quantity: element.quantity,
                                                variation_id: element.variation_id,
                                                vendor_id: element.vendor_id,
                                                name: element.name,
                                                id: element.id,
                                                total: element.subtotalprice,
                                                meta_data:addonsMetaData,
                                                addons:element.addons,
                                            });
                                        });
                                        });

                                        var payment_details = {
                                            method_id: this.pay_type,
                                            method_title: this.pay_type,
                                            paid: true,
                                            //transaction_id: this.pay_id,
                                        };

                                        var billing_address = billingAddress;
                                        var shipping_address = shippingAddress;
                                        this.order_data = {
                                            billing_address: this.billingAddress,
                                            amount:this.totalPrice,
                                            product_price:this.product_amount,
                                            shipping_price:this.shippingCost,
                                            sub_total:this.totalPrice - this.shippingCost,
                                            total:this.totalPrice,
                                            shipping_address: this.shippingAddress,
                                            line_items: line_items,
                                            shipping_lines: {
                                               id   : this.shipping_id,
                                               cost : this.shippingCost,
                                            },
                                            payment_details: payment_details,
                                        };
                                        console.log("Order details:: ", this.order_data);

                                    });
                                });
                        });
                } else {
                    this.id = this.navParams.get("order_id");
                    this.total = this.navParams.get("total");
                }
            });
    }

    slidesDidLoad(slides: IonSlides) {
        slides.startAutoplay();
    }
    ngOnInit() {
        this.storage.get("userService.id").then((val) => {
            //console.log("value", val);
            if (val) {
                //console.log(val);
                this.cust_id = val;
                console.log( this.cust_id)
                this.setupStripe();
               
            }
            this.storage.get("token").then((val) => {
                //console.log("value", val);
                if (val) {
                    //console.log(val);
                    this.loginToken = val;
                    console.log( this.loginToken)
                   
                }
            });
        });
      
    }

    close() {
        this.modalCtrl.dismiss();
    }

    async present() {
        this.isLoading = null;
        this.loadingCtrl
          .create({
            message: "Loading",
            mode:'ios'
          })
          .then((value) => {
            this.isLoading = value;
            this.isLoading.present();
          });
      }
    
      async dismiss() {
        this.isLoading.dismiss();
      }

    setupStripe() {
        let elements = this.stripe.elements();
        var style = {
            base: {
                color: "#32325d",
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#aab7c4",
                },
            },
            invalid: {
                color: "#fa755a",
                iconColor: "#fa755a",
            },
        };

        this.card = elements.create("card", { style: style, hidePostalCode: true });
        console.log(elements);
        console.log(this.card);
        this.card.mount("#card-element");
        this.card.addEventListener("change", (event) => {
            var displayError = document.getElementById("card-errors");
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = "";
            }
        });

        var form = document.getElementById("payment-form");
        var amount = document.getElementById("payment-amount");
        form.addEventListener("submit", (event) => {
            event.preventDefault();
            this.isButton = true;
            this.present();
            
            console.log($(amount).val());
            if ($(amount).val()) {
                this.errorShow = false;
                // this.cardAmount = $(amount).val();

                this.stripe.createSource(this.card).then(async (result) => {
                    this.isButton = false;
                    this.dismiss();
                    var loadingModal = await this.loadingCtrl.create({
                        spinner: 'bubbles',
                        message: "Please wait...",
                        translucent: true,
                        cssClass: "custom-class custom-loading",
                        mode:'ios'
                    });

                    if (result.error) {
                      
                        var errorElement = document.getElementById("card-errors");
                        errorElement.textContent = result.error.message;
                        loadingModal.present();
                        console.log(result.error.message)
                    } else {
                        console.log("THE SOURCE");
                        console.log(result);
                        if (result.source !== undefined && result.source.id !== undefined) {
                            this.cardToken = result.source.id;
                           

                            this.stripe.createToken(this.card).then(async (tokenResponse) => {
                                this.isButton = false;
                                if (tokenResponse.error) {
                                    var errorElement = document.getElementById("card-errors");
                                    errorElement.textContent = tokenResponse.error.message;
                                    console.log(tokenResponse.error.message)
                                } else {
                                    console.log(tokenResponse);
                                    if (tokenResponse.token !== undefined && tokenResponse.token.id !== undefined) {
                                        this.sourceToken = tokenResponse.token.id;

                                        loadingModal.present();
                                        this.cardDetail = {
                                            stripe_card_source: this.cardToken,
                                            customer_id: this.cust_id,
                                            // product_price : this.product_amount, 
                                            product_price : this.amount, 
                                            shipping_price: this.shipping_amount,
                                            amount: this.amount,
                                            order_data: this.order_data,
                                            token: this.loginToken,
                                            stripe_card_token: this.sourceToken,
                                            vendor_id: this.order_data.line_items[0].vendor_id
                                        };
                                        console.log(this.cardDetail);
                                        this.wooService
                                            .Payment(this.cardDetail)
                                            .subscribe(async (res: any) => {
                                                console.log("res", res);
                                                loadingModal.dismiss();
                                                this.isButton = false;
                                            // debugger;
                                            this.dismiss();
                                                if (res.status == "success") {
                                                   
                                                   // this.order_id = res.data.order_id;
                                                    this.storage.remove("oddwolves-cart");
                                                    this.storage.remove("total");
                                                    localStorage.removeItem('oddwolves-cart');
                                                    const toast = await this.toastController.create({
                                                        message: res.message,
                                                        duration: 2000,
                                                        color: "dark",
                                                    });
                                                    toast.present();
                                                    this.modalController.dismiss();
                                                    this.navCtrl.navigateRoot(["/orderlist"]);
                                                } else {
                                                   
                                                    const toast = await this.toastController.create({
                                                        message: 'error',
                                                        duration: 2000,
                                                        color: "dark",
                                                    });
                                                    toast.present();
                                                }
                                            }, async (e) => {
                                                const toast = await this.toastController.create({
                                                    message: e.message + e.code,
                                                    duration: 2000,
                                                    color: "dark",
                                                });
                                                toast.present();
                                                this.isButton = false;
                                                loadingModal.dismiss();

                                            });



                                    }
                                }
                            });



                        }
                    }
                });
            } else {
                this.errorShow = true;
            }
        });
    }
    
    // async presentLoading() {
    //     const loading = await this.loadingCtrl.create({
    //       cssClass: 'my-custom-class',
    //       message: 'Please wait...',
    //       duration: 2000,
    //       mode:'ios'
    //     });
    //     await loading.present();
    
    //     const { role, data } = await loading.onDidDismiss();
    //     console.log('Loading dismissed!');
    //   }

    validateAmount(event) {
        console.log(event);
        this.errorShow = false;
    }

}
