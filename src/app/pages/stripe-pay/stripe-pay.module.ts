import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { StripePayPage } from './stripe-pay';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    StripePayPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: StripePayPage
      }
    ])
  ],
})
export class StripePayPageModule { }
