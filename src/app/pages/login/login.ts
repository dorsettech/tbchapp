/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:09:49
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, AlertController, LoadingController, NavParams, ToastController } from '@ionic/angular';
import { UserService } from '../../providers/user-service';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { SowService } from '../../providers/sow-service';
import { AppConfig } from '../../app-config';
import { Storage } from '@ionic/storage';
import * as CryptoJS from 'crypto-js';
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { RegisterPage } from '../register/register';

import { ForgetPage } from '../forget/forget.page'
import { Events } from '../../providers/events'
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { from } from 'rxjs';
/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'app-page-login',
    templateUrl: 'login.html',
    styleUrls: ['login.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LoginPage {
    loginInfo: FormGroup;
    userName: string;
    password: string;
    email: string;
    errorModal: any;
    checkout: any;
    isModelShow: boolean = false;
    fieldTextType: boolean;
    isBackButton: boolean = true;
    error_messages = {

        'email': [
            { type: 'required', message: '*Email is required.' },
            { type: 'pattern', message: '*Please enter a valid email address.' }
        ],
        'password': [
            { type: 'required', message: '*Password is required.' },
            { type: 'minlength', message: '*Password should be minimum 6 characters.' },
            { type: 'maxlength', message: '*Password should be maximum 30 characters.' }
        ],
    }
    // public navParams = new NavParams()
    token: Promise<any>;
    id: Promise<any>;
    stripeKey: Promise<any>;
    constructor(public navCtrl: NavController, private formBuilder: FormBuilder, public events: Events, private router: Router, public route: ActivatedRoute, public userService: UserService, public wooService: WoocommerceService,
        public appConfig: AppConfig, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public sowService: SowService, public storage: Storage
        , public translateService: TranslateService, public toastController: ToastController, public modalCtrl: ModalController,
        // public navParams: NavParams
    ) {

        this.route.queryParams.subscribe(params => {
            console.log(params)
            this.checkout = params['value'];
            console.log(this.checkout)
            if (this.checkout == "login") {
                console.log("here")
                this.isModelShow = false;
                this.isBackButton = true;

            } else {
                console.log("here1")
                this.isModelShow = true;
                this.isBackButton = false;
            }
        })
        console.log(this.checkout)


        this.loginInfo = this.formBuilder.group({

            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),

            ])),
            password: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(30)
            ])),


        },
        );


    }

    doRegister() {
        this.modalCtrl.create({ component: RegisterPage }).then((value) => {
            value.present();
        });
        //this.navCtrl.navigateForward('register');
    }
    ionViewWillEnter() {
        this.loginInfo.reset();
    }
    doLogin() {
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK', 'Sign_In_Failed', 'Sign_In_No_Email', 'Sign_In_Email_Error', 'No_Email_Or_Password']).
            subscribe(async value => {
                var loadingModal = await this.loadingCtrl.create({
                    spinner: null,
                    message: "Please wait...",
                    translucent: true,
                    cssClass: "custom-class custom-loading",
                });
                loadingModal.present();
                if (this.appConfig.Eanble_Login_With_Password) {

                    if (this.loginInfo.valid) {
                        this.wooService.login('', { username: this.loginInfo.value['email'], password: this.loginInfo.value['password'] }).subscribe(async (data: any) => {
                            console.log(data)

                            if (data.success) {
                                this.token = this.storage.set("token", data.data.token);
                                this.stripeKey = this.storage.set("stripeKey", data.stripe_pk);
                                console.log(this.stripeKey)
                                this.modalCtrl.dismiss();
                                loadingModal.dismiss();
                                this.events.publish("logout", {})


                                console.log("1")
                                this.userService.id = data.data.id;
                                this.id = this.storage.set("userService.id", data.data.id);
                                console.log(this.userService.id)
                                this.userService.email = data.data.user_email;
                                this.userService.first_name = data.data.first_name;
                                this.userService.last_name = data.data.last_name;
                                this.userService.name = data.data.user_nicename;
                                this.userService.username = data.data.user_display_name;
                                this.userService.isAuthenticated = true;

                                await this.storage.set('oddwolves-user-info', data);
                                const toast = await this.toastController.create({
                                    message: "Successfully Logged In",
                                    duration: 2000,
                                    color: 'dark'
                                });
                                toast.present();

                                this.router.navigate(['main']);
                                this.events.publish("logout", {})

                            } else {
                                loadingModal.dismiss();
                                const toast = await this.toastController.create({
                                    // header: value['Notice'],
                                    message: data.message,
                                    color: 'dark',
                                    buttons: [{
                                        text: 'Ok',
                                        cssClass: 'secondary',
                                        handler: () => {
                                            this.loginInfo.reset();
                                        }
                                    }]
                                });
                                toast.present();

                            }
                        }, (reson) => {

                            loadingModal.dismiss();
                        });
                    } else {

                        for (const property in this.loginInfo.value) {

                            this.loginInfo.controls[property].markAsTouched();
                        }
                        this.loginInfo.markAsTouched();
                    }


                }
                else {
                    if (this.email == null || this.email.trim().length == 0) {

                        const toast = await this.toastController.create({
                            message: value["Sign_In_No_Email"],
                            duration: 2000,
                            color: 'dark'
                        });
                        toast.present();

                        return;
                    }
                    loadingModal.present();
                    this.wooService.getCustomerByEmail({ email: this.email }).subscribe(async (data: Array<any>) => {
                        if (data.length > 0 && data[0] && data[0].id > 0) {
                            this.userService.id = data[0].userId;
                            this.userService.email = data[0].user_email;
                            this.userService.first_name = data[0].first_name;
                            this.userService.last_name = data[0].last_name;
                            this.userService.name = data[0].user_nicename;
                            this.userService.username = data[0].user_display_name;
                            this.userService.isAuthenticated = true;
                            loadingModal.dismiss();
                            this.router.navigate(['main'])
                            this.modalCtrl.dismiss();
                        } else {
                            loadingModal.dismiss();

                            const toast = await this.toastController.create({
                                message: value["Sign_In_Email_Error"],
                                duration: 2000,
                                color: 'dark'
                            });
                            toast.present();
                        }
                    }, (reson) => {
                        loadingModal.dismiss();
                    });
                }
            });

    }


    onskip() {

        this.modalCtrl.dismiss();
    }
    toggleFieldTextType() {
        this.fieldTextType = !this.fieldTextType;
    }

    forgot() {
        this.modalCtrl.create({ component: ForgetPage }).then((value) => {
            value.present();
        });
    }
}
