/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 25/06/2021 - 09:42:18
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 25/06/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation, ViewChild } from "@angular/core";
import {
    NavController,
    LoadingController,
    AlertController,
    ModalController,
    ToastController
} from "@ionic/angular";
import { WoocommerceService } from "../../providers/woocommerce-service";
import {
    MenuController,
} from "@ionic/angular";
import { TbarService } from "../../providers/tbar-service";
import { SowService } from "../../providers/sow-service";
import { AppConfig } from "../../app-config";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { FormControl, FormGroup, FormBuilder, Validators, } from "@angular/forms";

/*
  Generated class for the Contact page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html',
    styleUrls: ['contact.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ContactPage {
    queryForm: FormGroup;

    error_messages = {
        'firstName': [
            { type: 'required', message: 'First Name is required.' },
        ],

        'lastName': [
            { type: 'required', message: 'Last Name is required.' },
        ],
        'email': [
            { type: 'required', message: 'Email is required.' },
            { type: 'pattern', message: 'Please enter a valid email address.' }
        ],
        'message': [
            { type: 'required', message: 'Message is required.' },
        ],

    }
    queryData: any = {};

    constructor(
        public navCtrl: NavController,
        public modalCtrl: ModalController,
        public appConfig: AppConfig,
        public loadingCtrl: LoadingController,
        public wooService: WoocommerceService,
        private router: Router,
        public tb: TbarService,
        public alertCtrl: AlertController,
        public sowService: SowService,
        public translateService: TranslateService,
        public menuCtrl: MenuController,
        public formBuilder: FormBuilder,
        public toastController: ToastController
    ) {
        this.queryForm = this.formBuilder.group({
            firstName: new FormControl('', Validators.compose([
                Validators.required
            ])),
            lastName: new FormControl('', Validators.compose([
                Validators.required
            ])),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),

            ])),
            message: new FormControl('', Validators.compose([Validators.required
            ])),
        });
    }

    close() {
        //this.modalCtrl.dismiss();
        this.menuCtrl.close()
    }
    openSidemenu() {
        this.menuCtrl.open();
    }

    onSubmit() {
        this.queryData = {
            "your-name": this.queryForm.value['firstName'] + this.queryForm.value['lastName'],
            "your-email": this.queryForm.value['email'],
            "your-message": this.queryForm.value['message'],

        }
        console.log("queryData:: ", this.queryData)
        this.wooService.postQuery(this.queryData).subscribe(async (response) => {
            console.log(response)
            var alert = await this.alertCtrl.create({
                message: "Your inquery has been submitted successfully. We will contact you back shortly",
                mode:'ios',
                buttons: [{
                    text: 'Okay',
                    handler: () => {
                        this.navCtrl.navigateForward("/main");
                    }
                }]
            });
            alert.present();
        },
            async (error) => {

                const toast = await this.toastController.create({
                    message: "Something went wrong",
                    duration: 2000,
                    color: "danger",
                });
                toast.present();
                console.log(error)
            }
        );

    }
    
}
