import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ContactPage } from './contact';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ContactPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: ContactPage
      }
    ])
  ],
})
export class ContactPageModule { }
