/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:12:00
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, AlertController, LoadingController } from '@ionic/angular';
import { UserService } from '../../providers/user-service';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { TranslateService } from '@ngx-translate/core';
import { ToastController } from '@ionic/angular';
import { SowService } from '../../providers/sow-service';
import { AppConfig } from '../../app-config';
import * as CryptoJS from 'crypto-js';
import {FormControl,FormGroup, FormBuilder,Validators,} from "@angular/forms";
/*
  Generated class for the Register page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
  selector: 'app-page-register',
  templateUrl: 'register.html',
  styleUrls: ['register.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterPage {
  // registInfo = {
  //   first_name: '',
  //   last_name: '',
  //   username: '',
  //   password: '',
  //   email: ''
  // };
  confirmPassword: '';
  errorModal: any;
  passwordCheck: boolean = false;
  fieldTextType:boolean;
  passwordType:boolean;
  params : any = [];
  registInfo : FormGroup;

  error_messages = {
    'first_name': [
      { type: 'required', message: '*First Name is required.' },
      { type: 'pattern', message: '*Please do not use special characters and numbers'  },
    ],
  
    'last_name': [
      { type: 'required', message: '*Last Name is required.' },
      { type: 'pattern', message: '*Please do not use special characters and numbers'  },
    ],
    'username': [
      { type: 'required', message: '*User Name is required.' },
    ],
   'email': [
      { type: 'required', message: '*Email is required.' },
       { type: 'pattern', message: '*Please enter a valid email address.' }
    ],
    'password': [
      { type: 'required', message: '*Password is required.' },
      { type: 'minlength', message: '*Password should be minimum 6 characters.' },
      { type: 'maxlength', message: '*Password should be maximum 30 characters.' },
      { type: 'pattern', message: 'Password must atleast 1 special charcter,uppercase and lowercase.' }
    ],
    
    'confirmPassword': [
      { type: 'required', message: 'Password is required.' },
    
      
    ],
  
  } 



  constructor(public navCtrl: NavController, 
    public viewCtrl: ModalController, 
    public userService: UserService, 
    public alertCtrl: AlertController,
    public wooService: WoocommerceService, 
    public loadingCtrl: LoadingController, 
    public translateService: TranslateService,
    public toastController: ToastController, 
    public sowService: SowService,
     public appConfig: AppConfig,
     public modalCtrl: ModalController,
     public formBuilder: FormBuilder, ) {
      this.registInfo = this.formBuilder.group({
        first_name: new FormControl('', Validators.compose([
          Validators.required, Validators.pattern('[A-Za-z]+'),
        ])),
        last_name: new FormControl('', Validators.compose([
          Validators.required, Validators.pattern('[A-Za-z]+')
        ])),
        username: new FormControl('', Validators.compose([
          Validators.required
        ])),
        email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
         
        ])),
        password: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(30),
          Validators.pattern(
            /(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}/
        )
        ])),
        confirmPassword: new FormControl('', Validators.compose([
          
         ])),
      
      }, 
      );
  }

  async close() {
    await this.modalCtrl.dismiss();
    //this.modalCtrl.dismiss();
  }

 async btnRegisterUser() {
    this.translateService.get(['Notice', 'NetWork_Error', 'OK', 'Loading', 'Require_Email', 'Require_Name',
      'Require_Username', 'Require_Password', 'Password_Check', 'Sing_up_Successed', 'Sign_up_failed', 'Contact_Admin']).subscribe(async value => {
    
        
          var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
        var customer = {

        };
       
          this.params ={
            'email': this.registInfo.value.email,
            'first_name': this.registInfo.value.first_name,
            'last_name': this.registInfo.value.last_name,
            'username': this.registInfo.value.username,
            'password': this.registInfo.value.password
          }
          // console.log(this.params)
        this.wooService.register('',this.params).subscribe(async (data: any) => {
          loadingModal.dismiss();
          console.log(data)
          if (data && data.status == "success") {
            const toast = await this.toastController.create({
              message: data.message,
              duration: 2000,
              color: 'dark'
            });
            toast.present();
            setTimeout(() => {
              this.viewCtrl.dismiss();
            }, 2000);
          } else {
            const toast = await this.toastController.create({
                message: data.message,
                duration: 2000,
                color: 'dark'
              });
              toast.present();
          }
        }, async (reason) => {
          loadingModal.dismiss();
        
          const toast = await this.toastController.create({
            message: value["Contact_Admin"],
            duration: 2000,
            color: 'dark'
          });
          toast.present();
        });
      });
  }





  vrifyConfirmpassword() {
    if (this.registInfo.value.confirmPassword != "") {
      console.log(
        this.registInfo.value.password,
        this.registInfo.value.confirmPassword
      );
      if (this.registInfo.value.password == this.registInfo.value.confirmPassword) {
        this.passwordCheck = false;
      } else {
        this.passwordCheck = true;
      }
    }
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleType() {
    this.passwordType = !this.passwordType;
  }
}
