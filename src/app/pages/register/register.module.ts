import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { RegisterPage } from './register';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RegisterPage
  ],
  imports: [
    IonicModule,CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: RegisterPage
      }
    ])
  ],
})
export class RegisterPageModule { }
