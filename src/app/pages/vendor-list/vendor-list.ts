import { Component, Renderer, ViewEncapsulation } from '@angular/core';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { SowService } from '../../providers/sow-service';
import { TbarService } from '../../providers/tbar-service';
import { TranslateService } from '@ngx-translate/core';
import { Keyboard } from '@ionic-native/keyboard/ngx';
/*
  Generated class for the VendorList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'app-page-vendor-list',
    templateUrl: 'vendor-list.html',
    styleUrls: ['vendor-list.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VendorListPage {
    vendors: any;
    searchInput: any;
    noResult: boolean;
    constructor(public navCtrl: NavController,
        public sowService: SowService, public tb: TbarService, public loadingCtrl: LoadingController, public alertCtrl: AlertController
        , public translateService: TranslateService, public renderer: Renderer, public keyboard: Keyboard) {
        this.noResult = false;
    }


    ionViewDidEnter() {
        this.loadAllVendor();
    }

   async loadAllVendor() {
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
             var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
            this.sowService.getVendorList().subscribe((data: Array<any>) => {
                if (data.length == 0) {
                    this.noResult = true;
                } else {
                    this.vendors = data;
                    this.noResult = false;
                }
                loadingModal.dismiss();
            }, (reson) => {
                loadingModal.dismiss();
                this.alertCtrl.create({
                    // header: value['Notice'],
                    message: value['NetWork_Error'],
                    buttons: [value['OK']]
                });
            });
        });
    }

    onClear() {
        this.loadAllVendor();
    }

    myOnSearch() {
        this.renderer.invokeElementMethod(event.target, 'blur');
        this.keyboard.hide();
        if (this.searchInput.replace(/^\s+|\s+$/g, "") == '') {
            return;
        }
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
              var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
            this.sowService.searchVendor({ 'key_word': this.searchInput }).subscribe((data: Array<any>) => {
                if (data.length == 0) {
                    this.noResult = true;
                } else {
                    this.vendors = data;
                    this.noResult = false;
                }
                loadingModal.dismiss();
            }, (reson) => {
                loadingModal.dismiss();
                this.alertCtrl.create({
                    // header: value['Notice'],
                    message: value['NetWork_Error'],
                    buttons: [value['OK']]
                });
            });
        });
    }

    onSearch() {
        this.renderer.invokeElementMethod(event.target, 'blur');
        this.keyboard.hide();
        if (this.searchInput.replace(/^\s+|\s+$/g, "") == '') {
            return;
        }
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
              var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
            this.sowService.searchVendor({ 'key_word': this.searchInput }).subscribe((data: Array<any>) => {
                if (data.length == 0) {
                    this.noResult = true;
                } else {
                    this.vendors = data;
                    this.noResult = false;
                }
                loadingModal.dismiss();
            }, (reson) => {
                loadingModal.dismiss();
                this.alertCtrl.create({
                    // header: value['Notice'],
                    message: value['NetWork_Error'],
                    buttons: [value['OK']]
                });
            });
        });
    }


    vendorSelected(vendor) {
        this.navCtrl.navigateForward('tabs/store/vendor/' + vendor.id);
    }
}
