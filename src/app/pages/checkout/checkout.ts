/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:07:17
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, LoadingController, AlertController, MenuController, ToastController } from '@ionic/angular';
import { Http } from '@angular/http';
import { AppConfig } from '../../app-config';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

import { UserService } from '../../providers/user-service';
import { Storage } from '@ionic/storage';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { FormControl, FormGroup, FormBuilder, Validators, } from "@angular/forms";
import { TbarService } from '../../providers/tbar-service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { ThanksPage } from '../thanks/thanks';
import { CountryPage } from '../country/country';
import { StripePayPage } from '../stripe-pay/stripe-pay';
// import * as csc from 'country-state-city';
import csc from 'country-state-city';
import { ICountry, IState, ICity } from 'country-state-city'
import * as $ from "jquery";

/*
  Generated class for the Checkout page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
// declare var csc: any;
@Component({
    selector: 'app-page-checkout',
    templateUrl: 'checkout.html',
    styleUrls: ['checkout.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CheckoutPage implements OnInit {
    shippingAddress: FormGroup;
    billingAddress: FormGroup;
    // shippingAddress = {
    //   first_name: '',
    //   last_name: '',
    //   email: '',
    //   phone: '',
    //   country: '',
    //   address_1: '',
    //   address_2: '',
    //   city: '',
    //   state: '',
    //   zip: ''
    // };


    errorMessage = {
        first_name: false,
        last_name: false,
        email: false,
        phone: false,
        country: false,
        address_1: false,
        address_2: false,
        city: false,
        state: false,
        postcode: false
    }
    // billingAddress = {
    //   first_name: '',
    //   last_name: '',
    //   email: '',
    //   phone: '',
    //   country: '',
    //   address_1: '',
    //   address_2: '',
    //   city: '',
    //   state: '',
    //   zip: '',
    //   emailValid :''
    // };
    selShipMethod = 0;
    total;
    desc = '';
    sameAddress = true;

    set_paid: boolean;
    errorModal: any;
    orderID = '';
    loadingModal: any;
    message: boolean = false;
    shipping_amount:any;
    error_messages = {
        'first_name': [
            { type: 'required', message: 'First Name is required.' },
        ],

        'last_name': [
            { type: 'required', message: 'Last Name is required.' },
        ],
        'email': [
            { type: 'required', message: 'Email is required.' },
            { type: 'email', message: 'Please enter a valid email address.' }
        ],
        'phone': [
            { type: 'required', message: 'Phone Number is required.' },
            { type: 'maxlength', message: 'Phone Number not more then 11  digits.' },
            { type: 'minlength', message: 'Phone Number should be minimum 8 digits.' },
        ],
        'address_1': [
            { type: 'required', message: 'Address is required.' },
        ],
        'country': [
            { type: 'required', message: 'Country is required.' },
        ],
        'state': [
            { type: 'required', message: 'State is required.' },
        ],
        'city': [
            { type: 'required', message: 'City is required.' },
        ],
        'postcode': [
            { type: 'required', message: 'Post Code is required.' },
            { type: 'pattern', message: 'Please enter valid post code.It can be alphanumeric only' },
        ],
    }
    selectedCountry :any;
    // ICountry: any;
    countries: any = [];
    states: any = [];
    billingStates: any = [];
    cities:any = [];
    billingCities:any = [];
    resp: any;
    email: any;
    shipping_id:any='';
    constructor(public navCtrl: NavController, public http: Http, public modalCtrl: ModalController, public storage: Storage, public appConfig: AppConfig, public formBuilder: FormBuilder,
        public userService: UserService, public wooService: WoocommerceService, public loadingCtrl: LoadingController, public alertCtrl: AlertController
        , public tbarService: TbarService, public translateService: TranslateService, public menuCtrl: MenuController, public inAppBrowser: InAppBrowser,
        private activatedRoute: ActivatedRoute, private payPal: PayPal, public toastController: ToastController,) {
        this.total = this.activatedRoute.snapshot.paramMap.get('total');
        this.desc = this.activatedRoute.snapshot.paramMap.get('description');
        this.shipping_amount = this.activatedRoute.snapshot.paramMap.get('shipping');
        this.shipping_id     = this.activatedRoute.snapshot.paramMap.get('shipping_id');
        this.sameAddress = true;
        this.selectedCountry = "United Kingdom" ;
        this.shippingAddress = this.formBuilder.group({
            first_name: new FormControl('', Validators.compose([
                Validators.required
            ])),
            last_name: new FormControl('', Validators.compose([
                Validators.required
            ])),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.email,

            ])),
            phone: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(11)
            ])),
            country: new FormControl('United Kingdom', Validators.compose([Validators.required
            ])),
            city: new FormControl('', 
            ),
            state: new FormControl('', 
            ),
            postcode: new FormControl('', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9 ]*$')
            ])),
            address_1: new FormControl('', Validators.compose([Validators.required
            ])),
            address_2: new FormControl('',),
        });
        this.billingAddress = this.formBuilder.group({
            first_name: new FormControl('', Validators.compose([
                Validators.required
            ])),
            last_name: new FormControl('', Validators.compose([
                Validators.required
            ])),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.email,

            ])),
            phone: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(11)
            ])),
            country: new FormControl('United Kingdom', Validators.compose([Validators.required
            ])),
            city: new FormControl('', ),
            state: new FormControl('', ),
            postcode: new FormControl('', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9 ]*$')
            ])),
            address_1: new FormControl('', Validators.compose([Validators.required
            ])),
            address_2: new FormControl('',),
        });
        this.storage.get('oddwolves-cart').then((data) => {
            
           console.log(data)
            });
            this.storage.get("oddwolves-user-info").then((val) => {
                //console.log("value", val);
                if (val) {
                    //console.log(val);
                    this.resp = val;
                    console.log( this.resp.data.email)
                    this.email = this.resp.data.email
                   this.shippingAddress.controls.email.setValue(this.email);
                 
                }
            });
        
    }

    ionViewWillEnter() {
        this.getAllCountries();
        // this.companyFormSelected()
        //this.getStatesOfCountry(countryCode);
        this.translateService.get(['Loading']).subscribe(async value => {
            var loadingModal = await this.loadingCtrl.create({
                message: value['Loading']
            });
            loadingModal.present();

            this.storage.get('oddwolves-user-shipping-address').then((data) => {
                console.log(data)
                console.log(this.userService)
                if (data) {
                    this.shippingAddress = data;
                } else {
                    if (this.userService.isAuthenticated == true) {
                       
                    }
                }loadingModal.dismiss();
            }).then(() => {
                this.storage.get('oddwolves-user-billing-address').then((data) => {
                    if (data) {
                        this.billingAddress = data;
                    } else {
                        if (this.userService.isAuthenticated == true) {
                           
                        }
                    }
                    loadingModal.dismiss();
                });
            });
        });

    }

    ngOnInit() {
        this.wooService.dismissloading();

        setTimeout(() => {
            this.getStatesOfCountry('shipping');
        }, 2000);
    }
    companyFormSelected(type) {
        if(this.countries.length) {
            if(type == 'shipping'){
            let value = this.countries.find(x => x.name == this.shippingAddress.controls.country.value);
            if(value){
                return value.name;
            }
        } 
        else{
                //console.log(this.billingAddress.controls.country.value)
                let value = this.countries.find(x => x.name == this.billingAddress.controls.country.value);
                if(value){
                    return value.name;
                }            }
        }
        return '';
    }
    getAllCountries(){
     
      this.countries = csc.getAllCountries();
      console.log(csc)

    }
    getStatesOfCountry(type){
       // let cCode:any = ''
        if(type == 'shipping'){
            this.states = [];
            this.shippingAddress.controls.state.setValue('');
            console.log(this.countries, this.shippingAddress.controls.country.value)
            let cCode =  this.countries.find(x => x.name == this.shippingAddress.controls.country.value);
            console.log(cCode)
             this.states  = csc.getStatesOfCountry(cCode.id);
        }else{
            this.billingStates = [];
            this.billingAddress.controls.state.setValue('');
            let cCode =  this.countries.find(x => x.name == this.billingAddress.controls.country.value);
             this.billingStates  = csc.getStatesOfCountry(cCode.id);
        }
    }
    getCitiesOfState(type){
        if(type == 'shipping'){
            this.cities  = [];
            this.shippingAddress.controls.city.setValue('');
            let sCode =  this.states.find(x => x.name == this.shippingAddress.controls.state.value);
            this.cities  = csc.getCitiesOfState(sCode.id)
        }else{
            this.billingCities = [];
            this.billingAddress.controls.city.setValue('');
            let sCode =  this.billingStates.find(x => x.name == this.billingAddress.controls.state.value);
             this.billingCities  = csc.getCitiesOfState(sCode.id)
        }
       
    }
    back() {
        this.navCtrl.back();
    }

    async selectCountry(type) {
        let countryModal = await this.modalCtrl.create({
            component: CountryPage, componentProps: {
                'country': type == 'shipping' ? this.shippingAddress.value.country : this.billingAddress.value.country,
                'forwhich': type
            }
        });
        countryModal.onDidDismiss().then((data) => {

            if (data.data.forwhich == 'shipping') {
                this.shippingAddress.controls.country.setValue(data.data.country);
                // this.shippingAddress.country = data.data.country;
            }
            else {
                this.billingAddress.controls.country.setValue(data.data.country);
            }
        });
        countryModal.present();
    }

    payViaSite() {
        this.translateService.get(['Loading', 'Notice', 'NetWork_Error', 'OK']).subscribe(value => {
            if (this.orderID == '') {
                var shipping_lines = new Array();
                shipping_lines = [{
                    'method_id': this.appConfig.Shop_Shipping[this.selShipMethod].id,
                    'method_title': this.appConfig.Shop_Shipping[this.selShipMethod].name,
                    'total': this.appConfig.Shop_Shipping[this.selShipMethod].cost
                }];
               
                var line_items = new Array();
                this.storage.get('oddwolves-cart').then((data) => {
                    var cartProductArray = JSON.parse(data);
                    cartProductArray.forEach(element => {
                        line_items.push({
                            'product_id': element.product_id,
                            'quantity': element.quantity,
                            'variation_id': element.variation_id,
                            'variations': element.variations
                        })
                    });

                    var billing_address = this.billingAddress;
                    var shipping_address = this.shippingAddress;
                    let order = {
                        billing_address,
                        'customer_id': this.userService.id, shipping_address, line_items, shipping_lines
                    };

                    this.wooService.createOrder('', { order }).subscribe((data: any) => {
                        if (data) {
                            // this.loadingModal.dismiss();
                            this.orderID = data.order.id;
                            this.hanlderPayViaSite();
                        }
                    }, async (reason) => {
                        // this.loadingModal.dismiss();

                        const toast = await this.toastController.create({
                            message: value["Network_Error"],
                            duration: 2000,
                            color: "danger",
                        });
                        toast.present();
                    });
                });
            } else {
                this.hanlderPayViaSite();
            }
        });
    }

    hanlderPayViaSite() {
        this.translateService.get(['Loading', 'Notice', 'Cancel_Order_Note', 'Pay_Order', 'Cancel_Order']).subscribe(value => {
            var browserObj = this.inAppBrowser.create(this.appConfig.Shop_URL + '/my-account/orders', '_blank', '');

            browserObj.on('exit').subscribe(
                () => {
                    this.loadingModal = this.loadingCtrl.create({
                        message: value['Loading']
                    });
                    this.loadingModal.present();
                    this.wooService.getOrder(this.orderID).subscribe(async (order: any) => {
                        this.loadingModal.dismiss();
                        if (order.status == 'pending') {
                            let confirm = await this.alertCtrl.create({
                                header: value['Notice'],
                                message: value['Cancel_Order_Note'],

                                buttons: [
                                    {
                                        text: value['Pay_Order'],
                                        handler: () => {
                                            confirm.dismiss();
                                        }
                                    },
                                    {
                                        text: value['Cancel_Order'],
                                        handler: async () => {
                                            var loadingModal = await this.loadingCtrl.create({
                                                message: value['Loading']
                                            });
                                            loadingModal.present();
                                            this.wooService.updateOrderStatus(this.orderID, 'cancelled').subscribe(() => {
                                                this.navCtrl.navigateRoot('');
                                                loadingModal.dismiss();
                                                this.orderID = '';
                                            });
                                        }
                                    }
                                ]
                            });
                            confirm.present();
                        } else if (order.status == 'processing') {
                            let modal = await this.modalCtrl.create({
                                component: ThanksPage,
                                componentProps: { "order_id": order.id, "total": order.total, "type": "site" }
                            });
                            modal.present();
                            this.navCtrl.navigateRoot('');
                            this.storage.remove('oddwolves-cart');
                            this.tbarService.cartBage = 0;
                        }
                    });
                },
                err => {
                });
        });

    }

    addressSame() {
        if (!this.sameAddress) {
            this.billingAddress.reset();
            this.billingAddress.controls.country.setValue('United Kingdom');
            this.billingAddress.controls.email.setValue(this.email);
            this.getStatesOfCountry('billing');
        }

    }

    payPayPal() {
        this.translateService.get(['Loading', 'Notice', 'Paypal_Pay_Error', 'Paypal_Configuration_Error', 'Paypal_Initialization_Error'])
            .subscribe(async (value) => {
                var loadingModal = await this.loadingCtrl.create({
                    message: value['Loading']
                });
                loadingModal.present();
                this.payPal.init({
                    "PayPalEnvironmentProduction": this.appConfig.PayPal_EnvironmentProduction,
                    "PayPalEnvironmentSandbox": this.appConfig.PayPal_EnvironmentSandbox
                }).then(() => {
                    this.payPal.prepareToRender(this.appConfig.Paypal_Environments, new PayPalConfiguration({})).then(() => {

                        let payment = new PayPalPayment((String)(Number.parseFloat(this.total) +
                            Number.parseFloat(this.appConfig.Shop_Shipping[this.selShipMethod].cost.toString())), this.appConfig.Shop_Currency, this.desc, 'sale');
                        this.payPal.renderSinglePaymentUI(payment).then(async (resp) => {
                            // Successfully paid
                            loadingModal.dismiss();
                            let modal = await this.modalCtrl.create({
                                component: ThanksPage,
                                componentProps: { "paypal_id": resp.response.id, "ship_method": this.selShipMethod, "type": "paypal" }
                            });
                            modal.present();
                            this.navCtrl.navigateRoot('');
                        }, () => {
                            // Error or render dialog closed without being successful
                            loadingModal.dismiss();

                        });
                    }, async () => {
                        // Error in configuration
                        loadingModal.dismiss();

                        const toast = await this.toastController.create({
                            message: value["Paypal_Configuration_Error"],
                            duration: 2000,
                            color: "danger",
                        });
                        toast.present();
                    });
                }, async () => {
                    loadingModal.dismiss();
                    // Error in initialization, maybe PayPal isn't supported or something else

                    const toast = await this.toastController.create({
                        message: value["Paypal_Intialization_Error"],
                        duration: 2000,
                        color: "danger",
                    });
                    toast.present();
                });
            });

    }




    payStripe() {
        console.log("dfgtgd", this.shippingAddress)
        if (this.shippingAddress.valid) {
            console.log("same", this.sameAddress)
            if (this.sameAddress) {
                this.billingAddress.patchValue(this.shippingAddress.value);
              //  this.navCtrl.navigateForward("stripe");
            //    this.toast();
            this.Payment();
                console.log("sameAddress", this.billingAddress)
            } else {
                console.log('this.billingAddress.valid:: ', this.billingAddress.valid, this.billingAddress);
                if (this.billingAddress.valid) {
                    // this.toast();
                   //this.navCtrl.navigateForward("stripe");
                    if (this.appConfig.Shop_Shipping[this.selShipMethod].cost) {

                    this.Payment();
                    } else {
                      this.message = true;
                    }
                } else {

                    for (const property in this.billingAddress.value) {

                        this.billingAddress.controls[property].markAsTouched();
                    }
                    this.billingAddress.markAsTouched();
                }
            }
        } else {
            for (const property in this.shippingAddress.value) {
                console.log(`${property}: ${this.shippingAddress[property]}`);
                this.shippingAddress.controls[property].markAsTouched();
            }


        }
    }


    async Payment() {
        console.log(this.total)
        console.log(this.desc)
        console.log(this.appConfig.Shop_Shipping)
        console.log(this.shippingAddress.value)
        let modal = await this.modalCtrl.create({
            component: StripePayPage, componentProps: {
                "total": Number.parseFloat(this.total),
                //  + Number.parseFloat(this.appConfig.Shop_Shipping[this.selShipMethod].cost.toString()),
                "desc": this.desc,
                "shipping":this.shipping_amount,
                'shipping_id':this.shipping_id,
               // "stripe_id": this.stripe_id,
                "ship_method": this.selShipMethod,
                "type": "stripe",
                 "shippingAddress": this.shippingAddress.value,
                 "billingAddress": this.billingAddress.value,
                "same": this.sameAddress
            }
        });

        modal.onDidDismiss().then(async (data) => {
            console.log(data)

            if (data.data  == true) {
                console.log( data.data.order_data.shipping_lines)
                let modalThanks = await this.modalCtrl.create({
                    component: ThanksPage, componentProps: {
                        "stripe_card_token": data.data.stripe_card_token,
                        "ship_method": data.data.order_data.shipping_lines,
                        "type": data.data.order_data.payment_details.method_title,
                        "shippingAddress": data.data.order_data.shipping_address,
                        "billingAddress": data.data.order_data.billingAddress,
                        "same": data.data.order_data.shipping_address
                    }
                 
                    
                });
                console.log(this.shippingAddress)
                console.log(this.billingAddress)
                console.log(this.sameAddress)
                modalThanks.present();
                this.navCtrl.navigateRoot('');
            }
        });
        modal.present();
    }


    async toast() {
        const toast = await this.toastController.create({
            message: "Stripe is not working Yet we will notify you soon",
            duration: 2000,
            color: "danger",
        });
        toast.present();
    }
}



























