import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CheckoutPage } from './checkout';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CheckoutPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: CheckoutPage
      }
    ])
  ],
})
export class CheckoutPageModule { }
