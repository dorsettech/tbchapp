/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:08:14
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, AlertController, LoadingController, NavParams, ToastController } from '@ionic/angular';
import { UserService } from '../../providers/user-service';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { SowService } from '../../providers/sow-service';
import { AppConfig } from '../../app-config';
import { Storage } from '@ionic/storage';
import * as CryptoJS from 'crypto-js';
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';

import { RegisterPage } from '../register/register';
import { Events } from '../../providers/events'
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-forget',
    templateUrl: './forget.page.html',
    styleUrls: ['./forget.page.scss'],
})
export class ForgetPage {

    userName: string;
    password: string;
    email: string;
    errorModal: any;
    checkout: any;
    isModelShow: boolean = false;
    fieldTextType: boolean;
    isBackButton: boolean = true;
    public navParams = new NavParams()
    RequestResetForm: any;
    error_messages = {
        email: [
            { type: "required", message: "Email is required." },
            { type: "pattern", message: "Please enter a valid email address." },
        ],
    };
    constructor(public navCtrl: NavController, public events: Events, private router: Router, public route: ActivatedRoute, public userService: UserService, public wooService: WoocommerceService,
        public appConfig: AppConfig, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public sowService: SowService, public storage: Storage
        , public translateService: TranslateService, public modalCtrl: ModalController,
        public toastController: ToastController) {

        this.route.queryParams.subscribe(params=>{
          console.log(params)
         this.checkout = params['value'];
         console.log(this.checkout)
         if(this.checkout == "login"){
          this.isModelShow = false;
          this.isBackButton = true;

        }else{
          this.isModelShow = true;
          this.isBackButton = false; 
        }
        })
        console.log(this.checkout)
        this.RequestResetForm = new FormGroup({
            email: new FormControl("", Validators.compose([Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"),])
            ),
        });



    }

    signin(){
        
        console.log("hrtt")
        this.router.navigate(['login'])
    }

    onskip() {
        this.modalCtrl.dismiss();
    }
   
    RequestResetUser() {
        const data = this.RequestResetForm.value;
        if (this.RequestResetForm.valid) {
            this.wooService.forgot('',data).subscribe(async (res: any) => {
                console.log("res", res);

                if (res.status == "success") {
                    const toast = await this.toastController.create({
                        message:res.message,
                        duration: 3000,
                        color: "dark",
                    });
                    toast.present();
                    this.modalCtrl.dismiss();
                    this.router.navigate(["login"]);
                } else {
                    const toast = await this.toastController.create({
                        message:res.message,
                        duration: 3000,
                        color: "dark",
                    });
                    toast.present();
                }
            });
        } else{
                        
            for (const property in this.RequestResetForm.value) {

                this.RequestResetForm.controls[property].markAsTouched();
            }
            this.RequestResetForm.markAsTouched();
            }
    }
}
