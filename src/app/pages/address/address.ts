import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, LoadingController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { UserService } from '../../providers/user-service';
import { TranslateService } from '@ngx-translate/core';
import { ToastController } from '@ionic/angular';
/*
  Generated class for the Address page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'app-page-address',
    templateUrl: 'address.html',
    styleUrls: ['address.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AddressPage {
    shippingAddress = {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        country: '',
        address_1: '',
        address_2: '',
        city: '',
        state: '',
        Post_Code: ''
    };
    billingAddress = {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        country: '',
        address_1: '',
        address_2: '',
        city: '',
        state: '',
        Post_Code: ''
    };
    sameAddress = true;
    loadingModal: any;
    constructor(public navCtrl: NavController, public storage: Storage, public modalCtrl: ModalController, public loadingCtrl: LoadingController,
        public userService: UserService, public menuCtrl: MenuController, public translateService: TranslateService, public toastController: ToastController) {

    }

    async selectCountry(type) {
        console.log(type)
        let countryModal = await this.modalCtrl.create({
            component: 'CountryPage',
            componentProps: {
                'country': type == 'shipping' ? this.shippingAddress.country : this.billingAddress.country,
                'forwhich': type
            }
        });
        const { data } = await countryModal.onDidDismiss();
        if (data.forwhich == 'shipping') {
            this.shippingAddress.country = data.country;
        }
        countryModal.present();
    }

    ionViewDidEnter() {
        this.translateService.get(['Loading']).subscribe(value => {
            var loadingModal = null;
            this.loadingCtrl.create({
                message: value['Loading']
            }).then((value) => {
                loadingModal = value;
                loadingModal.present();
                this.storage.get('oddwolves-user-shipping-address').then((data) => {
                    if (data) {
                        this.shippingAddress = data;
                    } else {
                        this.shippingAddress.email = this.userService.email;
                    }
                }).then(() => {
                    this.storage.get('oddwolves-user-billing-address').then((data) => {
                        if (data) {
                            this.billingAddress = data;
                        }
                        else {
                            this.billingAddress.email = this.userService.email;
                        }
                        loadingModal.dismiss();
                    });
                });
            });



        });
    }

    back() {
        this.navCtrl.back();
    }

    saveAddress() {
        this.translateService.get(['Address_Saved']).subscribe(async value => {
            const toast = await this.toastController.create({
                message: value['Address_Saved'],
                duration: 2000,
                color: 'dark'
            });
            toast.present();
            this.storage.set('oddwolves-user-shipping-address', this.shippingAddress);
            if (this.sameAddress) {
                this.storage.set('oddwolves-user-billing-address', this.shippingAddress);
            } else {
                this.storage.set('oddwolves-user-billing-address', this.billingAddress);
            }
        });
    }
    openSidemenu() {
        this.menuCtrl.open();
    }
}
