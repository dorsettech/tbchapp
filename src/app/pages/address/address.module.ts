import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { AddressPage } from './address';
import { TranslateModule,  } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AddressPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: AddressPage
      }
    ])
  ],
})
export class AddressPageModule { }
