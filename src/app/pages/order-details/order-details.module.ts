import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { OrderDetailsPage } from './order-details';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    OrderDetailsPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: OrderDetailsPage
      }
    ])
  ],
})
export class OrderDetailsPageModule { }
