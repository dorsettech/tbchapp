import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { WoocommerceService } from '../../providers/woocommerce-service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
/*
  Generated class for the OrderDetails page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'app-page-order-details',
    templateUrl: 'order-details.html',
    styleUrls: ['order-details.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OrderDetailsPage {
    loadingModal: any;
    order: any;
    errorModal: any;
    showList:Boolean=false;
    constructor(public navCtrl: NavController, public wooService: WoocommerceService,
        public loadingCtrl: LoadingController, public alertCtrl: AlertController,
        public translateService: TranslateService, private activatedRoute: ActivatedRoute,
        public toastController: ToastController) {

    }

    back() {
        this.navCtrl.back();
    }
close(){

}
openSidemenu(){
    
}
    ionViewDidEnter() {
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
                var loadingModal = await this.loadingCtrl.create({
                spinner: 'bubbles',
                message: "Please wait...",
                translucent: true,
                mode:'ios',
                cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
            this.wooService.getOrder(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((order) => {
                this.order = order;
               
                this.order.line_items.map((data,indexing)=>{
                    var arrayindex = data.meta_data;
                    var newarrayindex = [];
                            arrayindex.map((list,index)=>{
                                // debugger;
                                var key = list.key;
                                var firstchar =key.charAt(0);
                                if(firstchar != '_'){
                                    //this.order.line_items[indexing].meta_data.splice(index, 1);
                                    newarrayindex.push(list);
                                }
                            })
                            this.order.line_items[indexing].meta_data = newarrayindex;
                           

                        })
                
                console.log("order", this.order)
                loadingModal.dismiss();
            }, async (reason) => {
                loadingModal.dismiss();

                    const toast = await this.toastController.create({
                            message: "Oops! Network offline",
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
            });
        });
    }

    
   
}
