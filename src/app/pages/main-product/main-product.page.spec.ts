import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainProductPage } from './main-product.page';

describe('MainProductPage', () => {
  let component: MainProductPage;
  let fixture: ComponentFixture<MainProductPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainProductPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainProductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
