/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 02/07/2021 - 14:26:41
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 02/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import {
    NavController,
    LoadingController,
    AlertController,
    ToastController,
} from "@ionic/angular";
import { WoocommerceService } from "../../providers/woocommerce-service";
import {
  IonSlides,
  IonSlide,
  IonContent,
  MenuController,
} from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { TbarService } from "../../providers/tbar-service";
import { SowService } from "../../providers/sow-service";
import { AppConfig } from "../../app-config";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { Keyboard } from "@ionic-native/keyboard/ngx";

declare var window: any;

@Component({
  selector: "app-main-product",
  templateUrl: "./main-product.page.html",
  styleUrls: ["./main-product.page.scss"],
})
export class MainProductPage implements OnInit {
  @ViewChild("itemSlide") chatSlide: IonSlides;
  slideOpts = {
    slidesPerView: 3.6,
    autoplay: false,
  };
  @ViewChild("featured") featuredSlide: IonSlides;
  slideOpts1 = {
    slidesPerView: 2.2,
    autoplay: false,
  };
  @ViewChild("today") todaySlide: IonSlides;
  slideOpts2 = {
    slidesPerView: 2.2,
    autoplay: false,
  };
  slides: Array<any> = new Array<any>();
  showSlide: boolean = false;
  load_slide_end: boolean = false;
  lastProducts: Array<any> = new Array<any>();

  searchProducts: Array<any> = new Array<any>();
  searchInput: any = "";
  noSearchResult: boolean = false;
  sliderName: any;
  slideRow: any;
  sliderName2: any;
  slideRow2: any;
  sliderName3: any;
  slideRow3: any;
  slug: any;
  sliderHeading: any;
  slideImage: any;
  slideTitle: any;
  slideImage2: any;
  slideTitle2: any;
  slideTitle3: any;
  slideImage3: any;
  sliderSection1: any;
  sliderSection2: any;
  sliderSection3: any;
  sliderSection4:any;
  subHeading: any;
  sectionHeading1: any;
  sectionHeading2: any;
  sectionHeading3: any;
  sectionHeading4:any;
  sliderSubHeading: any;
  products: any;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public wooService: WoocommerceService,
    private router: Router,
    public tb: TbarService,
    public alertCtrl: AlertController,
    public sowService: SowService,
    public appConfig: AppConfig,
    public translateService: TranslateService,
    public menuCtrl: MenuController,
    public changeRef: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute, public toastController: ToastController
  ) {
    this.slug = this.activatedRoute.snapshot.paramMap.get("slug");
    let that = this;
    window.addEventListener("keyboardWillShow", () => {});
    window.addEventListener("keyboardWillHide", () => {
      that.searchProducts = [];
    });
  }

  openSidemenu() {
    this.menuCtrl.open();
  }

  ngOnInit() {
    //alert("vdshjvs");
  }

  ionViewWillEnter() {
    this.goToHomeSection();
  }

  gotoMenu() {
    this.navCtrl.navigateForward("/tabs/home");
  }
clearSearch(){

}
onSearch(event){
  
}
  doRefresh(refresher) {
    this.translateService
      .get(["Notice", "NetWork_Error", "OK"])
      .subscribe((value) => {
        this.slides = [];

        this.goToHomeSection();
        setTimeout(() => {
          refresher.target.complete();
        }, 5000);
      });
  }

  goToHomeSection() {
    
    this.translateService
      .get(["Notice", "Loading", "NetWork_Error", "OK"])
      .subscribe(async (value) => {
        var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
        this.wooService
          .getHomeProducts(this.slug)
          .subscribe((response: any) => {
            loadingModal.dismiss();
           // alert(JSON.stringify(response));
            console.log("Homeproducts:: ", JSON.stringify(response));
            this.sliderHeading = response.section_one.heading;
            this.sliderSubHeading = response.section_one.description;
            this.subHeading = response.section_one.subheading;
            this.sliderSection1 = response.all_slider_data.slider_one;
            this.sectionHeading1 = response.all_slider_data.slider_one[0].slider_title;
            console.log("sectionHeading1", this.sectionHeading1);
            this.sliderSection2 = response.all_slider_data.slider_two;
            this.sectionHeading2 = response.all_slider_data.slider_two[0].slider_title;
            console.log( this.sectionHeading2)
            this.sliderSection3 = response.all_slider_data.slider_three;
            this.sectionHeading3 = response.all_slider_data.slider_three[0].slider_title;
            this.sliderSection4 = response.all_slider_data.slider_four;
            this.sectionHeading4 = response.all_slider_data.slider_four[0].slider_title;
            console.log( this.sectionHeading4)
            console.log("home-product2", this.subHeading);
          }, async (error)=>{
                loadingModal.dismiss();
                  const toast = await this.toastController.create({
                            message: 'something went wrong',
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                 
          });
      });
  
  
  }

  openListings(product) {
    console.log(product)
    if (product.slug) {
      localStorage.setItem("Slug",product.slug)
      this.navCtrl.navigateForward("tabs/category/listing/" );
    } else {
      this.navCtrl.navigateForward("details/" + product.id);
    }
  }
  async AlertConfirm() {
    const alert = await this.alertCtrl.create({
      cssClass: "my-custom-class",
      message: "Oops! No Products Yet ",
      buttons: [
        {
          text: "Ok",

          cssClass: "secondary",
          handler: () => {},
        },
      ],
    });

    await alert.present();
  }
}
