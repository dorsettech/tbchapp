import { Component, ViewEncapsulation } from '@angular/core';

import { TbarService } from '../../providers/tbar-service';
import { Storage } from '@ionic/storage';
import { Router,NavigationExtras } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
/*
  Generated class for the Tabs tabs.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.html',
    styleUrls: ['tabs.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TabsPage {
    isSigin:boolean = false;


    constructor(private router: Router,  public storage: Storage,public navCtrl : NavController, public toastController:ToastController) {
        
    }
    onHome() {
        this.router.navigate(['main'])
    }
    ionViewDidEnter(){
        this.storage.get('oddwolves-user-info').then(async (val)=>{
            if(val != null){
               this.isSigin =true;
            }
        })
    }
    async signin_con(){
        
 
              const toast = await this.toastController.create({
                message: "Please Sign In First",
                duration: 2000,
                color: 'dark'
            });
            toast.present();
        
           
        
    
  }
}