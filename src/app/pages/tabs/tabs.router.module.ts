import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../home/home.module#HomePageModule'
          },
          {
            path: 'productdetails/:id',
            loadChildren: '../product-details/product-details.module#ProductDetailsPageModule'
          }
          // { path: 'details/:id', loadChildren: '../product-details/product-details.module#ProductDetailsPageModule' }
        ]
      },
      {
        path: 'category',
        children: [
          {
            path: '',
            loadChildren: '../category/category.module#CategoryPageModule'
          },
           { path: 'slug-url', 
            children: [
              {
                path: '',
                 loadChildren: '../slug-url/slug-url.module#SlugUrlPageModule' 
              },
             
            ]
         
          },
          
          {
            path: 'listing/:catID',
            children: [
              {
                path: '',
                loadChildren: '../listings/listings.module#ListingsPageModule'
              },
              {
                path: 'productdetails/:id',
                loadChildren: '../product-details/product-details.module#ProductDetailsPageModule'
              }
            ]

          },
         
          {
            path: 'productdetails/:id',
            loadChildren: '../product-details/product-details.module#ProductDetailsPageModule'
          }
        ]
      },
      {
        path: 'cart',
        children: [
          {
            path: '',
            loadChildren: '../cart/cart.module#CartPageModule'
          },
          {
            path: 'productdetails/:id',
            loadChildren: '../product-details/product-details.module#ProductDetailsPageModule'
          },
          {
            path: 'checkout',
            loadChildren: '../checkout/checkout.module#CheckoutPageModule'
          }
        ]
      },
      {
        path: 'store',
        children: [
          {
            path: '',
            loadChildren: '../vendor-list/vendor-list.module#VendorListPageModule'
          },
          {
            path: 'vendor/:id',
            loadChildren: '../vendor-store/vendor-store.module#VendorListPageModule'
          },
          {
            path: 'productdetails/:id',
            loadChildren: '../product-details/product-details.module#ProductDetailsPageModule'
          }
        ]
      },
      // {
      //   path: 'wishlist',
      //   children: [
      //     {
      //       path: '',
      //       loadChildren: '../wishlist/wishlist.module#WishlistPageModule'
      //     },
      //     {
      //       path: 'productdetails/:id',
      //       loadChildren: '../product-details/product-details.module#ProductDetailsPageModule'
      //     }
      //   ]
      // },
      {
        path: 'my',
        children: [
          {
            path: '',
            loadChildren: '../profile/profile.module#ProfilePageModule'
          },
          {
            path: 'orderlist',
            children: [
              {
                path: '',
                loadChildren: '../order-list/order-list.module#OrderListPageModule',
              },
              {
                path: 'orderdetails/:id',
                loadChildren: '../order-details/order-details.module#OrderDetailsPageModule',
              }
            ]
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
