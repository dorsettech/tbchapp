import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TabsPage } from './tabs';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { TabsPageRoutingModule } from './tabs.router.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    TabsPage
  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TranslateModule.forChild(),
    TabsPageRoutingModule
  ],
})
export class TabsPageModule { }
