import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlugUrlPage } from './slug-url.page';

describe('SlugUrlPage', () => {
  let component: SlugUrlPage;
  let fixture: ComponentFixture<SlugUrlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlugUrlPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlugUrlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
