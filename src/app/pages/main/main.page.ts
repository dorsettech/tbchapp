/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 30/04/2021 - 14:23:51
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 30/04/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import {
    NavController,
    LoadingController,
    AlertController,
    ToastController,
    ModalController,
} from "@ionic/angular";
import { WoocommerceService } from "../../providers/woocommerce-service";
import {
    IonSlides,
    IonSlide,
    IonContent,
    MenuController,
} from "@ionic/angular";
import { TbarService } from "../../providers/tbar-service";
import { SowService } from "../../providers/sow-service";
import { AppConfig } from "../../app-config";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { Storage } from "@ionic/storage";
import { isNgTemplate } from "@angular/compiler";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser/ngx";

declare var window: any;
@Component({
    selector: "app-main",
    templateUrl: "./main.page.html",
    styleUrls: ["./main.page.scss"],
})
export class MainPage implements OnInit {
    @ViewChild("itemSlide") chatSlide: IonSlides;
    slideOpts = {
        slidesPerView: 2.2,
        autoplay: false,
    };
    @ViewChild("featured") featuredSlide: IonSlides;
    slideOpts1 = {
        slidesPerView: 2.2,
        autoplay: false,
    };
    @ViewChild("today") todaySlide: IonSlides;
    slideOpts2 = {
        slidesPerView: 2.2,
        autoplay: false,
    };  
    @ViewChild("content") contentSlide: IonSlides;
    slideOpts3 = {
        slidesPerView: 2,
        autoplay: false,
        loop: true,
    };
    slides: Array<any> = new Array<any>();
    showSlide: boolean = false;
    load_slide_end: boolean = false;
    lastProducts: Array<any> = new Array<any>();
    isSearchShow: boolean = false;
    searchProducts: Array<any> = new Array<any>();
    searchInput: any = "";
    noSearchResult: boolean = false;
    sliderName: any;
    slideRow: any;
    sliderName2: any;
    slideRow2: any;
    sliderName3: any;
    slideRow3: any;
    length: any;
    item: any = [];
    item2: any = [];
    temp_token: any;
    options: any = {
        mode: 'ios'
    }
    searchBy: any = 'product';
    searchApiCheck: any;
    shops: any = [];
    noResults: string;
    product: any;
    slideRow4: any;
    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public wooService: WoocommerceService,
        private router: Router,
        public tb: TbarService,
        public alertCtrl: AlertController,
        public sowService: SowService,
        public appConfig: AppConfig,
        public translateService: TranslateService,
        public menuCtrl: MenuController,
        public changeRef: ChangeDetectorRef,
        public toastController: ToastController,
        public modal: ModalController,
        public storage: Storage,
        public splashScreen: SplashScreen,
        private iab: InAppBrowser
    ) {
        let that = this;
        window.addEventListener("keyboardWillShow", () => { });
        window.addEventListener("keyboardWillHide", () => {
            that.searchProducts = [];
        });
    }

    openSidemenu() {
        this.menuCtrl.open();
    }

    ngOnInit() {
        setTimeout(() => {
            this.splashScreen.hide();
        }, 6000);
    }

    ionViewWillEnter() {

        this.getSlider();
        this.getFeaturedCollection();
        this.getLastSlider();
        this.goToGiftSection();


        // this.modal.dismiss();
    }

    gotoMenu() {
        this.navCtrl.navigateForward("/tabs/home");
    }

    getSlider() {
        if (this.appConfig.Show_Home_Slide) {
            this.wooService.getHomeSlider().subscribe((data: Array<any>) => {
               
                this.slides = data;
      
                console.log("this.slides:: ", this.slides);
                this.changeRef.detectChanges();
            });
        }
    }

    getFeaturedCollection() { }

    getLastSlider() {
        this.wooService.getMainPageProducts().subscribe(
            (products: any) => {
                console.log("products:: ", products.data);
            if (products.data.length) {
                    this.lastProducts = products.data;
                    console.log("products length:: ", products.data.length);
                    this.length = Math.round(products.data.length / 2);
                    console.log("length::", this.length);
                    var that = this;
                    for (let i = 0; i < this.length; i++) {
                        that.item.push(this.lastProducts[i]);
                    }
                    console.log("item1::", that.item);
                    for (let j = this.length; j < products.data.length; j++) {
                        that.item2.push(this.lastProducts[j]);
                    }
                    console.log("item2::", that.item2);
                } else {
                    this.lastProducts = [];
                }
            },
            async (reason) => {
                this.lastProducts = [];
                console.log("reason:: ", reason);
                
            }
        );
    }

    doRefresh(refresher) {
        this.translateService
            .get(["Notice", "NetWork_Error", "OK"])
            .subscribe((value) => {
                this.slides = [];
                this.getSlider();
                this.getFeaturedCollection();
                this.getLastSlider();
                setTimeout(() => {
                    refresher.target.complete();
                }, 5000);
            });
    }

    viewProduct(id) {
        this.clearSearch();
        if (this.searchBy == 'product') {

            this.navCtrl.navigateForward("details/" + id);
        } else {
            this.navCtrl.navigateForward(["shop-products"], { queryParams: { id } });
        }
    }

    onSearch(value) {
      
        this.searchInput = value;
        if (this.searchInput.length) {
            this.translateService
                .get(["Notice", "Loading", "NetWork_Error", "OK"])
                .subscribe(async (value) => {
                    var loadingModal = await this.loadingCtrl.create({
                        message: value["Loading"],
                    });
                     //loadingModal.present();
                    if (this.searchApiCheck) {
                        this.searchApiCheck.unsubscribe();
                        //this.searchInput = "";
                        this.searchProducts = [];
                    }
                    if (this.searchBy == 'product') {
                        //loadingModal.dismiss();
                        this.getProducts()
                    } else {
                        if (!this.shops.length) {
                          
                            this.noSearchResult = true;

                           
                        } else {
                            //this.searchProducts = products.data;
                            //loadingModal.dismiss();
                            this.searchProducts = this.shops.filter(item => {
                                console.log(item)
                                return (item.vendor_shop_name.toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1);
                            });
                            console.log(this.searchProducts)
                        }
                    }
                    // loadingModal.present();

                });
        }
    }

   async getProducts() {
      
        this.searchApiCheck = this.wooService.getProducts({ search: this.searchInput }, this.searchBy).subscribe((products: any) => {
            

            if (this.searchBy == "product") {
                if (!products.length) {
                    this.noSearchResult = true;
                } else {

                    this.searchProducts = products;


                    console.log(this.searchProducts)
                }
            }

         
        },
            async (reason) => {
               
                this.translateService
                    .get(["Notice", "Loading", "NetWork_Error", "OK"])
                    .subscribe(async (value) => {
                        const toast = await this.toastController.create({
                            message: value["NetWork_Error"],
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                    });
            }
        );
    }

 async getVendors() {
     
        this.wooService.getProducts({ search: '' }, this.searchBy).subscribe((products: any) => {
         
            if (!products.data.length) {
                this.noSearchResult = true;
            } else {
                this.shops = products.data;
            }
           
        },
            async (reason) => {
               
                this.translateService
                    .get(["Notice", "Loading", "NetWork_Error", "OK"])
                    .subscribe(async (value) => {
                        const toast = await this.toastController.create({
                            message: value["NetWork_Error"],
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
                    });
            }
        );
    }

    clearSearch() {
        this.searchInput = "";
        this.searchProducts = [];
    }
    goToGiftSection() {
        this.wooService.getGiftProducts().subscribe(
            (response: any) => {
                console.log("products:: ", response);
                //alert(JSON.stringify(response));
                this.product = response
                this.sliderName = response.gift_ideas_for.Heading;
                this.slideRow = response.gift_ideas_for.gift_Ideas_For_You;
                this.sliderName2 = response.featured_collections.Heading;

                this.slideRow2 = response.featured_collections.Featured_collections;
                console.log(this.slideRow2)
                this.sliderName3 = response.Sell_british_craft_house.heading;
                this.slideRow3 = response.Sell_british_craft_house.Sell_British_Craft_House;
                console.log("home-product", this.sliderName);
                console.log("home-product2", this.slideRow);
                this.slideRow4 = response.threeSections
                console.log("data",this.slideRow4)
            },
            async (reason) => {
                this.lastProducts = [];
                console.log("reason:: ", reason);
                     const toast = await this.toastController.create({
                            message: "Oops! Network offline",
                            duration: 2000,
                            color: "dark",
                        });
                        toast.present();
            }
        );
    }
    Onproduct(slug) {

        if(slug == 'meet-the-maker'){
                    console.log("slug is ")
                    this.openWebpage()

        }
        else{
            if (slug) {
                if(slug == 'garden-exteriors') {
                    localStorage.setItem("Slug",'garden-exteriors')
                    this.navCtrl.navigateForward("tabs/category/listing/" );
                }else if(slug == 'fathers-day-gifts-and-cards'){
                    console.log("here")
                    localStorage.setItem("CAT_ID",slug);
                    this.navCtrl.navigateForward("tabs/category/slug-url");
                }  
                else{
                    this.navCtrl.navigateForward("main-product/" + slug);
                    console.log("sluggg",slug);
                }
               
            }  
            else {
                this.AlertConfirm();
                console.log("testing");
            }
        }
       
    }
 openWebpage() {
        const options: InAppBrowserOptions = {
            zoom: 'no'
        }
 const browser = this.iab.create('https://thebritishcrafthouse.co.uk/meet-the-maker/', '_system', options);

    }
    async AlertConfirm() {
        const toast = await this.toastController.create({
            message: "Oops ! No products yet",
            duration: 2000,
            color: "dark",
        });
        toast.present();
    }

    learn() {
        this.navCtrl.navigateForward("learn-more");
    }
    searchShow() {
        this.isSearchShow = !this.isSearchShow;
    }

}
