import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';
import { VendorStorePage } from './vendor-store';

@NgModule({
  declarations: [
    VendorStorePage
  ],
  imports: [

    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: VendorStorePage
      }
    ])
  ],
})
export class VendorListPageModule { }
