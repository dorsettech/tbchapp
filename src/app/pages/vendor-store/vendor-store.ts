import { Component, ViewEncapsulation } from '@angular/core';
import { NavController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { SowService } from '../../providers/sow-service';
import { AppConfig } from '../../app-config';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
/*
  Generated class for the VendorStore page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'app-page-vendor-store',
    templateUrl: 'vendor-store.html',
    styleUrls: ['vendor-store.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VendorStorePage {
    loadingModal: any;
    page: number;
    per_page: number;
    has_more: boolean;
    showType = 'grid';
    vendor: any;
    header_image: any;
    constructor(public navCtrl: NavController, public sowService: SowService, public appconfig: AppConfig, public alertCtrl: AlertController,
        public loadingCtrl: LoadingController, public translateService: TranslateService, public toastController: ToastController, private activatedRoute: ActivatedRoute) {
        this.page = 1;
        this.per_page = 10;
        this.has_more = true;
    }


    changeShwoType() {
        if (this.showType == 'grid') {
            this.showType = 'list';
        } else {
            this.showType = 'grid';
        }
    }

    ionViewDidEnter() {
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
              var loadingModal = await this.loadingCtrl.create({
          spinner: null,
          message: "Please wait...",
          translucent: true,
          cssClass: "custom-class custom-loading",
        });
        loadingModal.present();
            this.sowService.getVendorDetails(this.activatedRoute.snapshot.paramMap.get('id'), {
                page: 1, per_page: 10, is_vendor: true,
                'filter[author]': this.activatedRoute.snapshot.paramMap.get('id')
            }).subscribe((data) => {
                this.vendor = data;
                this.header_image = { 'backgroud-image': this.vendor.banner };
                if (this.vendor.products.length < this.per_page) {
                    this.has_more = false;
                }
                else {
                    this.page++;
                }
                loadingModal.dismiss();
            }, (reason) => {
                loadingModal.dismiss();
                this.toastController.create({
                    // header: value['Notice'],
                    message: value["NetWork_Error"],
                    duration: 2000,
                    color: 'dark'
                });
            });
        });

    }

    viewProduct(id) {
        this.navCtrl.navigateForward('details/' + id);
    }

    doRefresh(refresher) {
        this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
            this.page = 1;
            this.sowService.getVendorDetails(this.activatedRoute.snapshot.paramMap.get('id'), {
                page: 1, per_page: 10, is_vendor: true,
                'filter[author]': this.activatedRoute.snapshot.paramMap.get('id')
            }).subscribe((data) => {
                this.vendor = data;
                if (this.vendor.products.length < this.per_page) {
                    this.has_more = false;
                }
                else {
                    this.page++;
                }
                refresher.target.complete();;
            }, async (reson) => {
                refresher.target.complete();;

                const toast = await this.toastController.create({
                    message: value["NetWork_Error"],
                    duration: 2000,
                    color: 'dark'
                });
                toast.present();
            });
        });

    }

    doInfinite(infiniteScroll) {
        if (this.has_more) {
            this.translateService.get(['Notice', 'Loading', 'NetWork_Error', 'OK']).subscribe(async value => {
                this.sowService.getVendorDetails(this.activatedRoute.snapshot.paramMap.get('id'), {
                    page: this.page, per_page: this.per_page, is_vendor: true,
                    'filter[author]': this.activatedRoute.snapshot.paramMap.get('id')
                }).subscribe((data: any) => {
                    if (data && data.products) {
                        data.products.forEach(p => {
                            this.vendor.products.push(p);
                        });

                        if (data.products.length < this.per_page) {
                            this.has_more = false;
                        }
                        else {
                            this.page++;
                        }
                    }
                    infiniteScroll.target.complete();
                }, async (reason) => {
                    infiniteScroll.target.complete();
                    const toast = await this.toastController.create({
                        message: value["NetWork_Error"],
                        duration: 2000,
                        color: 'dark'
                    });
                    toast.present();
                });
            });

        }
        else {
            infiniteScroll.target.complete();
            infiniteScroll.target.disabled = true;
        }
    }

    back() {
        this.navCtrl.back();
        console.log("here")
    }
}
