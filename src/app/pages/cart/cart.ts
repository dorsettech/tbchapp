/**
 * @description      :
 * @author           : Betasoft
 * @group            :
 * @created          : 30/04/2021 - 14:23:42
 *
 * MODIFICATION LOG
 * - Version         : 1.0.0
 * - Date            : 30/04/2021
 * - Author          : Betasoft
 * - Modification    :
 **/
 import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnInit,
  ViewEncapsulation,
} from "@angular/core";
import {
  NavController,
  ModalController,
  LoadingController,
  AlertController,
  ToastController,
} from "@ionic/angular";
import { Storage } from "@ionic/storage";
import {
  WoocommerceService,
  CartProduct,
} from "../../providers/woocommerce-service";
import { TbarService } from "../../providers/tbar-service";
import { AppConfig } from "../../app-config";
import { UserService } from "../../providers/user-service";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { LoginPage } from "../login/login";
import { ActivatedRoute, Params } from "@angular/router";
import { MenuController } from "@ionic/angular";
import { element } from "protractor";

// import { debug } from "console";

/*
  Generated class for the CartTab page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: "app-page-cart",
  templateUrl: "cart.html",
  styleUrls: ["cart.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class CartPage {
  cart: Array<CartProduct>;
  productAmount:any=0.0;
  shippingAmount:any=0;
  totalAmount:any=0.0;
  shippingData:any=[];
  shippingDataId:any=[];
  selectShippingId:any = '';
  addonsMetaData:any=[];
  total: any = 0.0;
  subTotal: any;
  shipTotal: number;
  loadingModal: any;
  isEmpty: boolean;
  errorModal: any;
  productInCart: any;
  from: string;
  hideMe: boolean = false;
  showBack: boolean;
  showPrice: boolean = true;
  shipping_data: any;
  shipping_rule_cost: any;
  shipping: any;
  selectedShipping: any;
  currentquanrtity: any;
  currentquantity: number;
  shipping_rule_item_cost: any;
  method_title: any;
  shipping_details: any;
  shippingitemcost: any;
  finalquantity: any;
  Shipping_title:any;
  products: any[] = [];
  selectShippingdata:any =[];
  addonsPrize={};
  isLoading :any;
  productcount:any;
  newcart:any;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public storage: Storage,
    public tbarService: TbarService,
    public wooService: WoocommerceService,
    public loadingCtrl: LoadingController,
    public appConfig: AppConfig,
    public userService: UserService,
    public alertCtrl: AlertController,
    public translateService: TranslateService,
    public toastController: ToastController,
    public router: Router,
    private activatedRoute: ActivatedRoute,
    public menuCtrl: MenuController,
    public changeRef: ChangeDetectorRef,
    private zone: NgZone
  ) {
    this.isEmpty = false;
    this.tbarService.hideBar = false;
    this.showBack = true;
    if (this.router.url.includes("tabs")) {
      this.showBack = false;
    }
  }
  ionViewDidLoad() {
   
  // this.cartUpdate();
  }
  ionViewWillEnter() {
  
   // this.cartUpdate();
  }

  ionViewDidEnter() {
   
    this.cartData();
  }



  async present() {
    this.isLoading = null;
    this.loadingCtrl
      .create({
        message: "Loading",
        mode:'ios'
      })
      .then((value) => {
        this.isLoading = value;
        this.isLoading.present();
      });
  }

  async dismiss() {
    this.isLoading.dismiss();
  }

    cartData(){
    this.translateService.get(["Loading"]).subscribe((value) => {
      this.storage.get("oddwolves-cart").then((data) => {
        if (data) {
         
          this.cart = JSON.parse(data);
          console.log("this.cart");
          console.log(this.cart);
          console.log("this.cart");
          var cartData = JSON.parse(localStorage.getItem('oddwolves-cart'));
          if(cartData != null && cartData.length > 0){
            this.cart  = cartData;
          }
          
          if (this.cart.length > 0) {
            this.isEmpty = false;
            this.productAmount= 0;
            this.shippingAmount = 0;
            this.totalAmount    = 0; 
            var productcount = 0;
            
            this.present();
            this.cart.map(async (element,index) => {
              productcount++;
              this.productcount = productcount;
             var quantity = element.quantity;

                
            await this.wooService.shipingDetails({ product_id: element.product_id }).subscribe(
                (res: any) => {
                  res.forEach(element => {
                    
                     if(!this.shippingDataId.includes(element.id)){
                      this.shippingData.push({'id':element.id,'title':element.title,'shipping_rule_cost':element.shipping_rule_cost});
                      this.shippingDataId.push(element.id);
                     }
                    
                  });

                 var productprice           = parseFloat(element.price) * quantity;
                 
                 var productAddonsPrice     = this.updateAddonsPrice(element,quantity);
                 this.cart[index]['addons'] = productAddonsPrice.addonsmeta;
                 this.cart[index]['subtotalprice']  =  productprice + productAddonsPrice.totalprice ;
                 this.cart[index]['subtotal']  =  productprice + productAddonsPrice.totalprice ;
                 this.productAmount  += productprice + productAddonsPrice.totalprice;
                 
                 this.totalAmount    = this.productAmount ;

                },
              );
            });
            
            //this.newcart = this.cart;
           
           
            
            setTimeout(()=>{
              this.dismiss();
           
              localStorage.removeItem('oddwolves-cart');
              localStorage.setItem('oddwolves-cart',JSON.stringify(this.cart));
              
            },3000)
           
           
          } else {
            this.isEmpty = true;
            this.showPrice = false;
          }
        } else {
          this.isEmpty = true;
          this.showPrice = false;
        }
      });
    });
   
  }

  cartUpdate(productid:number,action:string){
          if (this.cart.length > 0) {
            this.productAmount  = 0;
            if(this.selectShippingId == '' && this.shippingAmount == 0){
              this.shippingAmount = 0;
            }
            this.totalAmount    = 0; 
            var productcount = 0;
          
            this.present();
            
           
            var cartData = localStorage.getItem('oddwolves-cart');
            if(cartData.length > 0){
              this.cart = JSON.parse(cartData);
            }

            this.cart.map(async (element,index) => {
              productcount++;
              this.productcount = productcount;
             var quantity = element.quantity;
              if(element.product_id == productid && productid !=undefined){
                 if(action == 'add'){
                  quantity = quantity + 1;
                  this.cart[index].quantity = quantity;
                 }else if(action == 'sub'){
                  quantity = quantity - 1;
                  this.cart[index].quantity = quantity;
                 }
              }

                 

             await this.wooService.shipingDetails({ product_id: element.product_id }).subscribe(
                (res: any) => {
                  res.forEach(element => {
                    
                    if(!this.shippingDataId.includes(element.id)){
                      this.shippingData.push({'id':element.id,'title':element.title,'shipping_rule_cost':element.shipping_rule_cost});
                      this.shippingDataId.push(element.id);
                    }
                    
                  });
              
                
                 var productprice           = parseFloat(element.price) * quantity;
                
                 var productAddonsPrice     = this.updateAddonsPrice(element,quantity);
                 this.cart[index]['addons'] = productAddonsPrice.addonsmeta; 
                 this.cart[index]['subtotalprice']  =  productprice + productAddonsPrice.totalprice ;
                 this.cart[index]['subtotal']  =  productprice + productAddonsPrice.totalprice ;
                 this.productAmount  += productprice + productAddonsPrice.totalprice;
                 
                 this.totalAmount    = this.productAmount + this.shippingAmount;

                },
              );
            });
           
            //this.shippingChnage(this.selectShippingId);
          } else {
            this.isEmpty = true;
            this.showPrice = false;
          }
          setTimeout(()=>{
            this.dismiss();

            localStorage.removeItem('oddwolves-cart');
            localStorage.setItem('oddwolves-cart',JSON.stringify(this.cart));
          },3000)
  }

   subQuantity(productid){
    
    this.cartUpdate(productid,'sub');
  }

   addQuantity(productid) {
   
    this.cartUpdate(productid,'add');
  }

  getShipping(product_id){

  }

  formatPrice(price) {
    return parseFloat(price).toFixed(2);
  }

  selectedShippingPrice(price) {
    return parseFloat(price).toFixed(2);
  }
  selectedSPrice() {
   return parseFloat(this.total) -  parseFloat(this.selectedShipping);
  }

  accMul(arg1, arg2) {
    var m = 0,
      s1 = arg1.toString(),
      s2 = arg2.toString();
    try {
      m += s1.split(".")[1].length;
    } catch (e) {}
    try {
      m += s2.split(".")[1].length;
    } catch (e) {}
    return (
      (Number(s1.replace(".", "")) * Number(s2.replace(".", ""))) /
      Math.pow(10, m)
    );
  }

  goHome() {
    this.router.navigateByUrl("");
  }

  remove(pid) {
    event.stopPropagation();
    var findIndex = this.cart.findIndex((element) => {
      return element.product_id == pid;
    });
    if (findIndex != -1) {
      this.cart.splice(findIndex, 1);
      if (this.cart.length == 0) {
        this.isEmpty = true;
        this.storage.remove("oddwolves-cart");
        localStorage.removeItem('oddwolves-cart');
      } else {
        localStorage.removeItem('oddwolves-cart');
        localStorage.setItem('oddwolves-cart',JSON.stringify(this.cart));
        this.storage.set("oddwolves-cart", JSON.stringify(this.cart));
      }
    }
    if(this.cart.length < 1){
      this.productAmount= 0.0;
      this.shippingAmount = 0.0;
      this.totalAmount = 0.0;
    }else{
      this.cartUpdate(undefined,undefined);
    }

  }

  shippingChnage(data){
    if(data != ''){
      this.shippingData.forEach(element => {
          if(element.id == data){
            this.shippingAmount = this.productcount * element.shipping_rule_cost;
          }
      });
      this.totalAmount = this.productAmount +  this.shippingAmount;
   }
  }

  inputNum() {
    event.stopPropagation();
  }

  async errorMsg(msg){
    const toast = await this.toastController.create({
        message: msg,
        duration: 2000,
        color: "dark",
    });
    toast.present();
}

async presentLoadingWithOptions() {
  const loading = await this.loadingCtrl.create({
   
    duration: 5000,
    message: 'Checking',
    translucent: true,
    cssClass: 'custom-class custom-loading',
    backdropDismiss: true,
    mode:'ios'
  });
  await loading.present();

  const { role, data } = await loading.onDidDismiss();
  console.log('Loading dismissed with role:', role);
}

  async checkout() {
   
    if(this.selectShippingId == '' || this.selectShippingId == 0 || this.selectShippingId == undefined){
      this.errorMsg('please select Shipping');
      return false;
    }
   //debugger;
    this.presentLoadingWithOptions();
    var desc = "";
    this.cart.forEach((element) => {
      desc += element.name + ",";
    });
    if (desc.length > 0) {
      desc = desc.substr(0, desc.length - 1);
    }

    if (this.userService.isAuthenticated == true) {
     
      desc = desc.substr(0, desc.length - 1);
      this.router.navigate([
        "checkout",
        { total: this.totalAmount,shipping:this.shippingAmount, description: desc ,shipping_id:this.selectShippingId},
      ]);
      // this.navCtrl.navigateForward(['checkout'], { queryParams: { "total": this.total, "description": desc }});
    } else {
     
      let modal = await this.modalCtrl.create({
        component: LoginPage,
        componentProps: { value: "checkout" },
      });

     
      modal.onDidDismiss().then(() => {
        this.wooService.presentloading();
        if (this.userService.isAuthenticated == true) {
         
          this.router.navigate([
            "checkout",
            { total: this.totalAmount,shipping:this.shippingAmount, description: desc ,shipping_id:this.selectShippingId},
          ]);
          // this.navCtrl.navigateForward(['checkout'], { queryParams: { "total": this.total, "description": desc }});
          this.wooService.dismissloading();
        }
        setTimeout(() => {
          this.wooService.dismissloading();
        }, 200);
      });
    }
  }

  viewProduct(id) {
    this.navCtrl.navigateForward("details/" + id);
  }

  back() {
    this.navCtrl.back();
  }
  openSidemenu() {
    this.menuCtrl.open();
  }
  async showConfirmAlert(item) {
    console.log(item);
    let alert = await this.alertCtrl.create({
      header: "Are you sure",
      message: "You want to remove this product from cart?",
      mode:'ios',
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
        {
          text: "Yes",
          handler: () => {
            this.remove(item.product_id);
          },
        },
      ],
    });

    await alert.present();
  }
  term() {
    this.productInCart.forEach((element) => {
      this.method_title = element.method_title;
      this.storage.set("method_title", JSON.stringify(this.method_title));
    });
  }


  hide() {
    this.hideMe = true;
  }

  updateAddonsPrice(element,quantity){
      
      let productPrice = parseFloat(element.price);
      let total        = 0;
      let addons       = [];
      element.addons_data.map((item,index)=>{
       
          if(item.type == 'checkbox'){
             
              if(item.name in element.addons_data_checkbox){
                let array = element.addons_data_checkbox[item.name];
                let price = 0;
                item.options.map((data,indexing)=>{ 
                    if(array.includes(data.label)){
                      //  if(data.price_type == 'flat_fee'){
                      //     total  += parseFloat(data.price);
                      //  }else if(data.price_type == 'quantity_based'){
                      //     total  += quantity * parseFloat(data.price);
                      //  }else if(data.price_type == 'percentage_based'){
                      //     total  +=  productPrice * parseFloat(data.price) / 100;
                      //  }
                      if(data.price == ''){
                        data.price = 0;
                      }

                      price += parseFloat(data.price);
                      total  += quantity * parseFloat(data.price);
                      addons.push({'key':item.name,'price':data.price,'value':data.label});
                    }
                })
               
              }
          }else if(item.type == 'custom_text'){
           
            if(item.name in element.addons_data_select){
              const selectvalue = element.addons_data_select[item.name];
              if(item.price == ''){
                item.price = 0;
              }
                if(selectvalue != '' && item.price != ''){
                  //  if(item.price_type == 'flat_fee'){
                  //     total  += parseFloat(item.price);
                  //  }else if(item.price_type == 'quantity_based'){
                  //     total  += quantity * parseFloat(item.price);
                  //  }else if(item.price_type == 'percentage_based'){
                  //     total  +=  productPrice * parseFloat(item.price) / 100;
                  //  }
                  addons.push({'key':item.name,'price':item.price,'value':selectvalue});
                  total  += quantity * parseFloat(item.price);
                }else{
                  addons.push({'key':item.name,'price':'','value':selectvalue});
                  total  += quantity * parseFloat(item.price);
                }
            }  
          }else{
            if(item.name in element.addons_data_select){
              const selectvaluedata = element.addons_data_select[item.name];
              item.options.map((data,indexing)=>{
              
                if(data.label == selectvaluedata){
                  //  if(data.price_type == 'flat_fee'){
                  //     total  += parseFloat(data.price);
                  //  }else if(data.price_type == 'quantity_based'){
                  //     total  += quantity * parseFloat(data.price);
                  //  }else if(data.price_type == 'percentage_based'){
                  //     total  +=  productPrice * parseFloat(data.price) / 100;
                  //  }
                  if(data.price == ''){
                    data.price = 0;
                  }

                   addons.push({'key':item.name,'price':data.price,'value':selectvaluedata});
                   total  += quantity * parseFloat(data.price);
                }
            })
            } 
          }
      });


      this.addonsPrize[element.product_id] = total;

      return {'totalprice':total , 'addonsmeta':addons}; 
  }
}
