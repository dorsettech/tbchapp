import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CartPage } from './cart';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CartPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: CartPage
      }
    ])
  ],
})
export class CartPageModule { }
