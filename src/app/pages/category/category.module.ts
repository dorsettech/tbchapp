import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CategoryPage } from './category';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms'; 

@NgModule({
  declarations: [
    CategoryPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: CategoryPage
      }
    ])
  ],
})
export class CategoryPageModule { }
