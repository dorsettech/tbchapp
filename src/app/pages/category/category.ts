/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 03/06/2021 - 13:28:26
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 03/06/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, ViewEncapsulation } from "@angular/core";
import {
    NavController,
    LoadingController,
    AlertController,
    ToastController,
} from "@ionic/angular";
import { WoocommerceService } from "../../providers/woocommerce-service";
import { TbarService } from "../../providers/tbar-service";
import { TranslateService } from "@ngx-translate/core";
import { MenuController } from "@ionic/angular";
/*
  Generated class for the Category page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: "app-page-category",
    templateUrl: "category.html",
    styleUrls: ["category.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class CategoryPage {
    items: any;
    page: any;
    per_page: any;
    categories: Array<any>;
    has_more: boolean;
    load_products_end: boolean;
    constructor(
        public navCtrl: NavController,
        public wooService: WoocommerceService,
        public loadingCtrl: LoadingController,
        public tbarService: TbarService,
        public alertCtrl: AlertController,
        public translateService: TranslateService,
        public menuCtrl: MenuController,
        public toastController: ToastController
    ) {

        this.categories = new Array<any>();
    }

    async showLoading() {
        const loading = await this.loadingCtrl.create({
            spinner: null,
            duration: 5000,
            message: "Please wait...",
            translucent: true,
            cssClass: "custom-class custom-loading",
        });
        return await loading.present();
    }
    ionViewWillEnter() {
        this.page = 1;
        this.per_page = 100;
        this.has_more = true;

        this.getCategories()
    }

    ngOnInit() {
        
    }

   
    openListings(cat, type, ind, set_value, first_value) {

        if (type == 'sub') {

            console.log("data==", JSON.stringify(cat.slug));
            if (ind == 0) {

                localStorage.setItem("Slug", cat.slug);

                this.navCtrl.navigateForward("tabs/category/listing/");
            }
        } else {
  
            var string=JSON.stringify(set_value)


            var result = string.split('/')
            var final = result[result.length - 2];
            first_value = first_value.replace(" ", "-");
            var res = first_value.concat(final);
            if (cat.slug == '') {

                var string = JSON.stringify(set_value)

           var result = string.split('/')
           var final = result[result.length -2];
            first_value=first_value.replace(" ","-");
            var res = first_value.concat(final);
            //alert(res);
              // cat.slug=res;
               ///alert( cat.slug);
             }
        
            if(cat.slug==''){
                localStorage.setItem("CAT_ID",final);
               this.navCtrl.navigateForward("tabs/category/slug-url");
            }
            else{
               
                localStorage.setItem("Slug",cat.slug);
                this.navCtrl.navigateForward("tabs/category/listing/");
            }
           


        }

    }
    openSidemenu() {
        this.menuCtrl.open();
    }




    getCategories() {
        this.translateService.get(["Notice", "Loading", "NetWork_Error", "OK"]).subscribe(async (value) => {
            var loadingModal = await this.loadingCtrl.create({
                spinner: null,
                message: "Please wait...",
                translucent: true,
                cssClass: "custom-class custom-loading",
            });
            loadingModal.present();
            this.wooService.getStoreCategories({ page: this.page, per_page: this.per_page }).subscribe((categories: Array<any>) => {
                console.log('category: ', categories);
                console.log('category:', categories)
                loadingModal.dismiss();
                this.categories = categories;
                console.log('data=', JSON.stringify(this.categories));
                if (categories.length < this.per_page) {
                    this.has_more = false;
                } else {
                    this.page++;
                }
                this.load_products_end = true;
               
            },
                async (reason) => {
                    loadingModal.dismiss();
                    const toast = await this.toastController.create({
                        message: "Oops! Network offline",
                        duration: 2000,
                        color: "dark",
                    });
                    toast.present();
                }
            );
        });


    }
    doInfinite(infiniteScroll) {
        this.translateService
            .get(["Notice", "NetWork_Error", "OK"])
            .subscribe((value) => {
                if (this.has_more) {
                    this.wooService
                        .getStoreCategories({ page: this.page, per_page: this.per_page })
                        .subscribe(
                            (categories: Array<any>) => {
                                categories.forEach((p) => {
                                    this.categories.push(p);
                                });

                                if (categories.length < this.per_page) {
                                    this.has_more = false;
                                } else {
                                    this.page++;
                                }
                                infiniteScroll.target.complete();
                            },
                            async (reason) => {
                                infiniteScroll.target.complete();
                                const toast = await this.toastController.create({
                                    message: "Network Error",
                                    duration: 2000,
                                    color: "dark",
                                });
                                toast.present();
                            }
                        );
                } else {
                    infiniteScroll.target.complete();
                    infiniteScroll.target.disabled = true;
                }
            });
    }



}
