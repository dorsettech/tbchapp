import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { AboutPage } from './about';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'; 
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AboutPage
  ],
  imports: [
    IonicModule,CommonModule,FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: AboutPage
      }
    ])
  ],
})
export class AboutPageModule {}

