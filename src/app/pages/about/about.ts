import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavController, ModalController, MenuController } from '@ionic/angular';
import { AppConfig } from '../../app-config';
/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'app-page-about',
    templateUrl: 'about.html',
    styleUrls: ['about.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AboutPage {

    constructor(public navCtrl: NavController, public menuCtrl: MenuController, public modalCtrl: ModalController, public appConfig: AppConfig) {

    }


    openSidemenu() {
        this.menuCtrl.open();
    }
}
