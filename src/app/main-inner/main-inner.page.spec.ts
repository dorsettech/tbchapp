import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainInnerPage } from './main-inner.page';

describe('MainInnerPage', () => {
  let component: MainInnerPage;
  let fixture: ComponentFixture<MainInnerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainInnerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainInnerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
