/**
    * @description      : 
    * @author           : Betasoft
    * @group            : 
    * @created          : 06/07/2021 - 09:01:03
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 06/07/2021
    * - Author          : Betasoft
    * - Modification    : 
**/
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import {
    NavController,
    LoadingController,
    AlertController,
} from "@ionic/angular";
import { WoocommerceService } from "../providers/woocommerce-service";
import {
    IonSlides,
    IonSlide,
    IonContent,
    MenuController,
} from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { TbarService } from "../providers/tbar-service";
import { SowService } from "../providers/sow-service";
import { AppConfig } from "../app-config";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { Keyboard } from '@ionic-native/keyboard/ngx';

declare var window: any;

@Component({
    selector: 'app-main-inner',
    templateUrl: './main-inner.page.html',
    styleUrls: ['./main-inner.page.scss'],
})
export class MainInnerPage implements OnInit {

    @ViewChild("itemSlide") chatSlide: IonSlides;
    slideOpts = {
        slidesPerView: 3.6,
        autoplay: false,
    };
    @ViewChild("featured") featuredSlide: IonSlides;
    slideOpts1 = {
        slidesPerView: 2.2,
        autoplay: false,
    };
    @ViewChild("today") todaySlide: IonSlides;
    slideOpts2 = {
        slidesPerView: 2.2,
        autoplay: false,
    };
    slides: Array<any> = new Array<any>();
    showSlide: boolean = false;
    load_slide_end: boolean = false;
    lastProducts: Array<any> = new Array<any>();

    searchProducts: Array<any> = new Array<any>();
    searchInput: any = "";
    noSearchResult: boolean = false;
    sliderName: any;
    slideRow: any;
    sliderName2: any;
    slideRow2: any;
    sliderName3: any;
    slideRow3: any;
    slug: any;

    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public wooService: WoocommerceService,
        private router: Router,
        public tb: TbarService,
        public alertCtrl: AlertController,
        public sowService: SowService,
        public appConfig: AppConfig,
        public translateService: TranslateService,
        public menuCtrl: MenuController,
        public changeRef: ChangeDetectorRef,
        private activatedRoute: ActivatedRoute,
    ) {
        this.slug = this.activatedRoute.snapshot.paramMap.get("slug");
        let that = this;
        window.addEventListener('keyboardWillShow', () => {
        });
        window.addEventListener('keyboardWillHide', () => {
            that.searchProducts = [];
        });
    }

    openSidemenu() {
        this.menuCtrl.open();
    }

    ngOnInit() { }

    ionViewWillEnter() {

        this.goToHomeSection;
    }

    gotoMenu() {
        this.navCtrl.navigateForward("/tabs/home");
    }


    doRefresh(refresher) {
        this.translateService
            .get(["Notice", "NetWork_Error", "OK"])
            .subscribe((value) => {
                this.slides = [];

                this.goToHomeSection();
                setTimeout(() => {
                    refresher.target.complete();
                }, 5000);
            });
    }

onSearch(event){

}
clearSearch(){

}


    goToHomeSection() {
        this.wooService.getHomeProducts(this.slug).subscribe(
            (response: any) => {
                console.log("Homeproducts:: ", response);
            },
            async (reason) => {
                this.lastProducts = [];
                console.log("reason:: ", reason);
            }
        );
    }
}

